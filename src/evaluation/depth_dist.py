import os

import numpy as np

from src.detection.mediapipe_pose_detector import MPModel
from src.enums.mp_enum import MPPoseLandmark
from src.evaluation.eval import read_camera_model_data, create_plotting_data, read_camera_model_axis, \
    get_mocap_data_specific, PlotData, visualize_data_axis_specific, TIME_SHIFT_NT
from src.utils.path import get_root_path


def show_depth_dist(lm=MPPoseLandmark.LEFT_ANKLE, show: bool = True, save: bool = True):
    axis = ['z']  # User variable
    mocap_axis = 'z'  # User variable

    _, camera_model_data_diff = read_camera_model_data(cameras, models, baby, age)
    camera_model_axis = read_camera_model_axis(cameras, models, axis, 0, -1, 1000, camera_model_data_diff, lm)

    # Prepare the data for plotting
    plot_data = create_plotting_data(cameras, models, axis, camera_model_axis)

    # Add also Mocap data
    mocap_data = get_mocap_data_specific(baby, age, "good", lm, mocap_axis, time_shift=TIME_SHIFT_NT)
    plot_data.append(PlotData(data=mocap_data, label=f'Mocap_{mocap_axis}', color='g', marker='X'))

    # Cut all data to the same length as Mocap data
    min_len = min([len(d.data) for d in plot_data])
    plot_data = [PlotData(data=d.data[:min_len], label=d.label, color=d.color, marker=d.marker) for d in plot_data]

    # make all data integers
    for d in plot_data:
        for i in range(len(d.data)):
            d.data[i] = int(d.data[i])

    for cam in cameras:
        root_path = f"{get_root_path()}/output/{baby}/{age}/{cam}/images/{lm.name}"
        # if the path does not exist, create it
        if not os.path.exists(root_path):
            os.makedirs(root_path)

        # Visualize the data
        if save:
            all_out_path = f"{root_path}/{lm.name}_depth_dist_mm_from_origin.png"
            visualize_data_axis_specific(out_path=all_out_path, data=plot_data, lm=lm,
                                         y_label="Z in mm")

        if show:
            visualize_data_axis_specific(data=plot_data, lm=lm, y_label="Z in mm")


def longest_same_value_interval(data):
    start = 0
    end = 0
    current_start = 0
    for i in range(1, len(data)):
        if data[i] != data[i - 1]:
            if i - 1 - current_start > end - start:
                end = i - 1
                start = current_start
            current_start = i
    if len(data) - 1 - current_start > end - start:
        end = len(data) - 1
        start = current_start
    return start, end


def shift_depth_to_mocap_and_show(cam, lm, show: bool = True, save: bool = True):
    axis = ['z']  # User variable
    mocap_axis = 'z'  # User variable

    _, camera_model_data_diff = read_camera_model_data([cam], models, baby, age)
    camera_model_axis = read_camera_model_axis([cam], models, axis, 0, -1, 1000, camera_model_data_diff, lm)

    # Prepare the data for plotting
    plot_data = create_plotting_data([cam], models, axis, camera_model_axis)

    # Add also Mocap data
    mocap_data = get_mocap_data_specific(baby, age, "good", lm, mocap_axis, time_shift=TIME_SHIFT_NT)
    mocap_len = len(mocap_data)

    # Find the start and end of the longest interval in Mocap data with the same value
    start, end = longest_same_value_interval(mocap_data)

    print(f"Camera: {cam}, landmark:{lm.name}")
    # print(f"Start: {start}, End: {end}")

    # Find the averaged offset on the interval and shift the data to the Mocap data
    for d in plot_data:
        offset = np.mean(np.array(d.data[start:end]) - np.array(mocap_data[start:end]))
        d.data -= offset

    diff_data = []
    # Also compute the final diff
    for d in plot_data:
        data = d.copy()
        data.data = np.abs(d.data[:mocap_len] - mocap_data)
        diff_data.append(data)

    for idx, d in enumerate(diff_data):
        model = models[idx]
        mean = np.mean(d.data)
        std = np.std(d.data)
        print(f"{model}: {mean:.2f} +/- {std:.2f}")

    plot_data.append(PlotData(data=mocap_data, label=f'Mocap_{mocap_axis}', color='g', marker='X'))

    # Cut all data to the same length as Mocap data
    min_len = min([len(d.data) for d in plot_data])
    plot_data = [PlotData(data=d.data[:min_len], label=d.label, color=d.color, marker=d.marker) for d in plot_data]

    # make all data integers
    for d in plot_data:
        for i in range(len(d.data)):
            d.data[i] = int(d.data[i])

    for d in diff_data:
        for i in range(len(d.data)):
            d.data[i] = int(d.data[i])

    root_path = f"{get_root_path()}/output/{baby}/{age}/{cam}/images/{lm.name}"
    # if the path does not exist, create it
    if not os.path.exists(root_path):
        os.makedirs(root_path)

    # Visualize the data
    if save:
        all_out_path = f"{root_path}/{lm.name}_depth_dist_mm_shifted_to_mocap.png"
        visualize_data_axis_specific(out_path=all_out_path, data=plot_data, lm=lm,
                                     y_label="Z in mm")
        all_out_path = f"{root_path}/{lm.name}_depth_diff_mm_shifted_to_mocap.png"
        visualize_data_axis_specific(out_path=all_out_path, data=diff_data, lm=lm,
                                     y_label="Z in mm")

    if show:
        visualize_data_axis_specific(data=plot_data, lm=lm, y_label="Z in mm")
        visualize_data_axis_specific(data=diff_data, lm=lm, y_label="Z in mm")


if __name__ == "__main__":
    baby = "NT"
    age = "27w"
    cameras = ["ZED", "D435", "D455"]  # User variable
    # cameras = ["D435"]  # User variable
    models = [MPModel.HEAVY, MPModel.FULL, MPModel.LITE, MPModel.VITPOSE]  # User variable

    show = False
    save = not show

    # for lm in MPPoseLandmark:
    #     if lm.is_mocap():
    #         show_depth_dist(lm, show, save)

    for cam in cameras:
        for lm in MPPoseLandmark:
            if lm.is_mocap():
    #             show_depth_dist(lm, show, save)
                shift_depth_to_mocap_and_show(cam, lm, show, save)

    # lm = MPPoseLandmark.LEFT_SHOULDER
    # show_depth_dist(lm, show, save)
    # shift_depth_to_mocap_and_show(cameras[0], lm, show, save)

    # for cam in cameras:
    #     shift_depth_to_mocap_and_show(cam, lm, show, save)
