import os

import numpy as np

from src.detection.mediapipe_pose_detector import MPModel
from src.enums.mp_enum import MPPoseLandmark
from src.evaluation.eval import read_camera_model_data, read_camera_model_axis, get_mocap_data_specific, TIME_SHIFT_NT, \
    PlotData, visualize_data_axis_specific
from src.evaluation.mp_3d_dist import normalize
from src.utils.path import get_root_path


def show_euclid_3d_diff(lm=MPPoseLandmark.LEFT_ANKLE, show: bool = True, save: bool = True, x_min=0, x_max=-1):
    axis = ['x', 'y', 'z']  # User variable
    base_gain = 1000  # User variable

    _, camera_model_data_diff = read_camera_model_data([camera], models, baby, age)
    camera_model_axis = read_camera_model_axis([camera], models, axis, 1, -1, base_gain, camera_model_data_diff, lm)

    # Add also Mocap data
    mocap_data_x = get_mocap_data_specific(baby, age, "good", lm, 'x', time_shift=TIME_SHIFT_NT)
    mocap_data_y = get_mocap_data_specific(baby, age, "good", lm, 'y', time_shift=TIME_SHIFT_NT)
    mocap_data_z = get_mocap_data_specific(baby, age, "good", lm, 'z', time_shift=TIME_SHIFT_NT)
    mocap_len = len(mocap_data_x)

    # Prepare the camera data for plotting
    model_distances = {}
    for model in models:
        x = np.array(camera_model_axis[camera][model]['x'])[:mocap_len] - np.array(
            mocap_data_y)  # Watch out for the axis
        y = np.array(camera_model_axis[camera][model]['y'])[:mocap_len] - np.array(
            mocap_data_x)  # Watch out for the axis
        z = np.array(camera_model_axis[camera][model]['z'])[:mocap_len] - np.array(
            mocap_data_z)
        dist = np.sqrt(x ** 2 + y ** 2 + z ** 2)
        model_distances[model] = dist

    # make all data integers
    for model in models:
        model_distances[model] = model_distances[model].astype(int)

    colors = ['b', 'r', 'k', 'y', 'c', 'm']
    color_idx = 0
    # Prepare the data for plotting
    plot_data = []
    for model in models:
        plot_data.append(PlotData(data=model_distances[model], label=f'{camera}_{model}', color=colors[color_idx]))
        color_idx += 1

    root_path = f"{get_root_path()}/output/{baby}/{age}/{camera}/images/{lm.name}"
    # if the path does not exist, create it
    if not os.path.exists(root_path):
        os.makedirs(root_path)

    if x_max != -1:
        plot_data = [PlotData(data=d.data[x_min:x_max], label=d.label, color=d.color) for d in plot_data]

    # Visualize the data
    if save:
        all_out_path = f"{root_path}/{lm.name}_3D_diff_mm_from_Mocap.png"
        visualize_data_axis_specific(out_path=all_out_path, data=plot_data, lm=lm,
                                     y_label="Euclidean Distance in mm")

    if show:
        visualize_data_axis_specific(data=plot_data, lm=lm, y_label="Euclidean Distance in mm")


def show_euclid_3d_dist(lm=MPPoseLandmark.LEFT_ANKLE, show: bool = True, save: bool = True, x_min=0, x_max=-1):
    axis = ['x', 'y', 'z']  # User variable

    _, camera_model_data_diff = read_camera_model_data([camera], models, baby, age)
    camera_model_coord_axis = read_camera_model_axis([camera], models, axis, 1, -1, 1000, camera_model_data_diff, lm)

    # Prepare the camera data for plotting
    model_distances_coord = {}
    for model in models:
        x = np.array(camera_model_coord_axis[camera][model]['x'])
        y = np.array(camera_model_coord_axis[camera][model]['y'])
        z = np.array(camera_model_coord_axis[camera][model]['z'])
        dist = np.sqrt(x ** 2 + y ** 2 + z ** 2)
        model_distances_coord[model] = dist

    # Add also Mocap data
    mocap_data_x = get_mocap_data_specific(baby, age, "good", lm, 'x', time_shift=TIME_SHIFT_NT)
    mocap_data_y = get_mocap_data_specific(baby, age, "good", lm, 'y', time_shift=TIME_SHIFT_NT)
    mocap_data_z = get_mocap_data_specific(baby, age, "good", lm, 'z', time_shift=TIME_SHIFT_NT)
    mocap_dist = np.sqrt(np.array(mocap_data_x) ** 2 + np.array(mocap_data_y) ** 2 + np.array(mocap_data_z) ** 2)

    # make all data integers
    for model in models:
        model_distances_coord[model] = model_distances_coord[model].astype(int)
    mocap_dist = mocap_dist.astype(int)

    colors = ['b', 'r', 'k', 'y', 'c', 'm']
    color_idx = 0
    # Prepare the data for plotting
    plot_coord_data = []
    for model in models:
        plot_coord_data.append(
            PlotData(data=model_distances_coord[model], label=f'{camera}_{model}', color=colors[color_idx]))
        color_idx += 1
    plot_coord_data.append(PlotData(data=mocap_dist, label=f'Mocap', color='g', marker='X'))

    # Cut all data to the same length as Mocap data
    min_len = min([len(d.data) for d in plot_coord_data])
    plot_coord_data = [PlotData(data=d.data[:min_len], label=d.label, color=d.color, marker=d.marker) for d in
                       plot_coord_data]

    if x_max != -1:
        plot_coord_data = [PlotData(data=d.data[x_min:x_max], label=d.label, color=d.color, marker=d.marker) for d in
                           plot_coord_data]

    root_path = f"{get_root_path()}/output/{baby}/{age}/{camera}/images/{lm.name}"
    # if the path does not exist, create it
    if not os.path.exists(root_path):
        os.makedirs(root_path)

    # Visualize the data
    if save:
        all_out_path = f"{root_path}/{lm.name}_3D_dist_mm_from_origin.png"
        visualize_data_axis_specific(out_path=all_out_path, data=plot_coord_data, lm=lm,
                                     y_label="Euclidean Distance in mm")

    if show:
        visualize_data_axis_specific(data=plot_coord_data, lm=lm, y_label="Euclidean Distance in mm")


def show_euclid_3d_dist_norm(lm=MPPoseLandmark.LEFT_ANKLE, show: bool = True, save: bool = True, x_min=0, x_max=-1):
    axis = ['x', 'y', 'z']  # User variable

    _, camera_model_data_diff = read_camera_model_data([camera], models, baby, age)
    camera_model_coord_axis = read_camera_model_axis([camera], models, axis, 5, -1, 1000, camera_model_data_diff, lm)

    # Prepare the camera data for plotting
    model_distances_coord = {}
    for model in models:
        x = np.array(camera_model_coord_axis[camera][model]['x'])
        y = np.array(camera_model_coord_axis[camera][model]['y'])
        z = np.array(camera_model_coord_axis[camera][model]['z'])
        dist = np.sqrt(x ** 2 + y ** 2 + z ** 2)
        model_distances_coord[model] = normalize(dist)

    # Add also Mocap data
    mocap_data_x = get_mocap_data_specific(baby, age, "good", lm, 'x', time_shift=TIME_SHIFT_NT+5)
    mocap_data_y = get_mocap_data_specific(baby, age, "good", lm, 'y', time_shift=TIME_SHIFT_NT+5)
    mocap_data_z = get_mocap_data_specific(baby, age, "good", lm, 'z', time_shift=TIME_SHIFT_NT+5)
    mocap_dist = np.sqrt(np.array(mocap_data_x) ** 2 + np.array(mocap_data_y) ** 2 + np.array(mocap_data_z) ** 2)
    mocap_dist = normalize(mocap_dist)

    colors = ['b', 'r', 'k', 'y', 'c', 'm']
    color_idx = 0
    # Prepare the data for plotting
    plot_coord_data = []
    for model in models:
        plot_coord_data.append(
            PlotData(data=model_distances_coord[model], label=f'{camera}_{model}', color=colors[color_idx]))
        color_idx += 1
    plot_coord_data.append(PlotData(data=mocap_dist, label=f'Mocap', color='g', marker='X'))

    # Cut all data to the same length as Mocap data
    min_len = min([len(d.data) for d in plot_coord_data])
    plot_coord_data = [PlotData(data=d.data[:min_len], label=d.label, color=d.color, marker=d.marker) for d in
                       plot_coord_data]

    if x_max != -1:
        plot_coord_data = [PlotData(data=d.data[x_min:x_max], label=d.label, color=d.color, marker=d.marker) for d in
                           plot_coord_data]

    root_path = f"{get_root_path()}/output/{baby}/{age}/{camera}/images/{lm.name}"
    # if the path does not exist, create it
    if not os.path.exists(root_path):
        os.makedirs(root_path)

    # Visualize the data
    if save:
        all_out_path = f"{root_path}/{lm.name}_3D_norm_dist_mm_from_origin.png"
        visualize_data_axis_specific(out_path=all_out_path, data=plot_coord_data, lm=lm,
                                     y_label="Normalized Distance")

    if show:
        visualize_data_axis_specific(data=plot_coord_data, lm=lm, y_label="Normalized Distance")


if __name__ == "__main__":
    baby = "NT"
    age = "27w"
    camera = "D435"  # User variable
    models = [MPModel.HEAVY, MPModel.FULL, MPModel.LITE, MPModel.VITPOSE]  # User variable

    show = True
    save = not show

    for lm in MPPoseLandmark:
        if lm.is_mocap():
            # show_euclid_3d_dist(lm, show, save)
            # show_euclid_3d_diff(lm, show, save)
            show_euclid_3d_dist_norm(lm, show, save)

    # x_min = 350
    # x_max = -1
    # lm = MPPoseLandmark.LEFT_HIP
    # show_euclid_3d_dist(lm, show, save, x_min, x_max)
    # show_euclid_3d_diff(lm, show, save, x_min, x_max)
