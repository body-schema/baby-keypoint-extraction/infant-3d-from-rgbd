from typing import List

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pydantic import BaseModel

from src.detection.mediapipe_pose_detector import MPModel
from src.enums.mp_enum import MPPoseLandmark
from src.utils.path import get_root_path


class PlotData(BaseModel):
    data: List[float]
    label: str
    color: str = 'b'
    marker: str = 'o'


TIME_SHIFT_NT = 122  # To Mocap
TIME_SHIFT_FI = 180

SHIFT_Y_D455 = 50  # To Mocap


def visualize_data_axis_specific(data: [PlotData], lm=MPPoseLandmark.LEFT_ANKLE, y_label='z', x_min=None, x_max=None,
                                 out_path: str = None, vis_x_min=325, vis_x_max=400):
    fontsize = 12
    plt.figure(num=1, figsize=(15, 7))
    for d in data:
        # plt.scatter(range(len(d.data)), d.data, marker=d.marker, color=d.color, label=d.label)
        plt.plot(d.data, marker=d.marker, color=d.color, label=d.label, linestyle='-', linewidth=1, markersize=4)
    plt.title(f'{lm.name} Landmark', fontsize=fontsize)
    plt.xlabel('Frame', fontsize=fontsize)
    plt.ylabel(f'{y_label}', fontsize=fontsize)
    plt.grid()
    plt.legend(fontsize=fontsize)
    plt.xticks(np.arange(0, len(data[0].data) + 25, 25), fontsize=fontsize)
    plt.yticks(fontsize=fontsize)

    # Add a light color transparent background
    if vis_x_max != -1:
        plt.axvspan(vis_x_min, vis_x_max, facecolor='grey', alpha=0.4)

    # minimum = min([min(d.data) for d in data]) if y_min is None else y_min
    # maximum = max([max(d.data) for d in data]) if y_max is None else y_max
    # y_shift = 2 if y_shift is None else y_shift
    # plt.ylim(minimum, maximum)
    # plt.yticks(np.arange(minimum, maximum, y_shift))
    if out_path:
        plt.savefig(out_path)
        plt.close()
    else:
        plt.show()


def get_data_axis_specific(data: pd.DataFrame | str, lm=MPPoseLandmark.LEFT_ANKLE, axis='z', time_from: int = 0,
                           time_to: int = -1, gain: int = 1) -> List[float]:
    if isinstance(data, str):
        data = pd.read_csv(data)
    data = data[data['landmark'] == lm.name]
    data.loc[:, axis] = data[axis] * gain
    data = data[axis].tolist()[time_from:time_to]

    # Nan values are replaced with 0
    data = [0 if np.isnan(d) else d for d in data]

    # Make the data integers
    # data = [int(d) for d in data]
    return data


def get_mocap_data_specific(baby: str, age: str, prefix: str, lm=MPPoseLandmark.LEFT_ANKLE, axis='z',
                            time_shift: int = 0,
                            gain: int = 1) -> List[float]:
    parent_folder = f"{get_root_path()}/resources/{baby}/{age}/mocap"
    data_file = f"{parent_folder}/{prefix}_part_downsampled.csv"
    data = pd.read_csv(data_file)
    mocap_column = f'{lm.to_mocap()}_{axis}'
    data.loc[:, mocap_column] = data[mocap_column] * gain
    data = data[mocap_column].tolist()[time_shift:]

    # Make the data integers
    data = [int(d) for d in data]
    return data


def get_camera_path_specific(camera_name: str, baby: str, age: str, model: MPModel):
    parent_folder = f"{get_root_path()}/output/{baby}/{age}/{camera_name}"
    if model == MPModel.VITPOSE:
        data_file = f"{parent_folder}/{model}/vitpose_out.csv"
        diff_file = f"{parent_folder}/{model}/vitpose_out_diff.csv"
    elif model == MPModel.NATIVE:
        data_file = f"{parent_folder}/{model}/{camera_name}_short_{model}.csv"
        diff_file = f"{parent_folder}/{model}/{camera_name}_short_{model}_diff.csv"
    else:
        data_file = f"{parent_folder}/{model}/{camera_name}_short_{model}.csv"
        diff_file = f"{parent_folder}/{model}/{camera_name}_short_{model}_diff.csv"
    return data_file, diff_file


def read_camera_model_data(cameras, models, baby, age):
    camera_model_data = {}
    camera_model_data_diff = {}
    for camera in cameras:
        camera_model_data[camera] = {}
        camera_model_data_diff[camera] = {}
        for model in models:
            data_file, diff_file = get_camera_path_specific(camera, baby, age, model)
            camera_model_data[camera][model] = pd.read_csv(data_file)
            camera_model_data_diff[camera][model] = pd.read_csv(diff_file)

    return camera_model_data, camera_model_data_diff


def read_camera_model_axis(cameras: List, models: List, axis: List, time_from: int, time_to: int, base_gain: int,
                           camera_model_data_diff: dict, lm: MPPoseLandmark):
    camera_model_axis = {}
    for camera in cameras:
        camera_model_axis[camera] = {}
        for model in models:
            camera_model_axis[camera][model] = {}
            for a in axis:
                gain = -base_gain if a == 'z' else base_gain
                if camera == "ZED" and a == 'y':
                    gain = -gain

                camera_model_axis[camera][model][a] = get_data_axis_specific(camera_model_data_diff[camera][model],
                                                                             lm=lm, axis=a, time_from=time_from,
                                                                             time_to=time_to, gain=gain)
    return camera_model_axis


def create_plotting_data(cameras, models, axis, camera_model_axis):
    colors = ['b', 'r', 'k', 'y', 'c', 'm']
    markers = ['o', '*', '+', 'x', 'D', '|', '_']
    color_idx = 0
    marker_idx = 0

    # Prepare Camera data for plotting
    plot_data = []
    for camera in cameras:
        for model in models:
            for a in axis:
                plot_data.append(PlotData(data=camera_model_axis[camera][model][a], marker=markers[marker_idx],
                                          label=f'{camera}_{model}', color=colors[color_idx]))
                color_idx += 1
                color_idx %= len(colors)
        marker_idx += 1
        marker_idx %= len(markers)

    return plot_data


"""
PLOTTING DATA #############################
"""


def show_data_axis_specific(lm=MPPoseLandmark.LEFT_ANKLE):
    baby = "NT"
    age = "27w"
    cameras = ["ZED"]  # User variable
    models = [MPModel.VITPOSE, MPModel.HEAVY]  # User variable
    axis = ['y']  # User variable
    mocap_axis = 'x'  # User variable
    base_gain = 1000  # User variable

    _, camera_model_data_diff = read_camera_model_data(cameras, models, baby, age)
    camera_model_axis = read_camera_model_axis(cameras, models, axis, 0, -1, base_gain, camera_model_data_diff, lm)

    # Prepare the data for plotting
    plot_data = create_plotting_data(cameras, models, axis, camera_model_axis)

    # Add also Mocap data
    mocap_data = get_mocap_data_specific(baby, age, "good", lm, mocap_axis, time_shift=TIME_SHIFT_NT)
    plot_data.append(PlotData(data=mocap_data, label=f'Mocap_{mocap_axis}', color='g', marker='X'))

    # Cut all data to the same length as Mocap data
    min_len = min([len(d.data) for d in plot_data])
    plot_data = [PlotData(data=d.data[:min_len], label=d.label, color=d.color, marker=d.marker) for d in plot_data]

    # Visualize the data
    visualize_data_axis_specific(plot_data, lm, y_label=f'{axis[0]} in mm')


def show_depth_differences(lm=MPPoseLandmark.LEFT_ANKLE, show: bool = True, save: bool = True):
    baby = "NT"
    age = "27w"
    camera = "D435"  # User variable
    models = [MPModel.LITE]  # User variable
    axis = ['z']  # User variable
    mocap_axis = 'z'  # User variable
    base_gain = 1000  # User variable
    time_from = 1
    time_to = 600

    _, camera_model_data_diff = read_camera_model_data([camera], models, baby, age)
    camera_model_axis = read_camera_model_axis([camera], models, axis, time_from, time_to, base_gain,
                                               camera_model_data_diff, lm)
    plot_data = create_plotting_data([camera], models, axis, camera_model_axis)

    # Add also Mocap data
    mocap_data = get_mocap_data_specific(baby, age, "good", lm, mocap_axis, time_shift=TIME_SHIFT_NT)
    plot_data.append(PlotData(data=mocap_data, label=f'Mocap_{mocap_axis}', color='g', marker='X'))

    root_path = f"{get_root_path()}/output/{baby}/{age}/{camera}/images/{lm.name}"
    # if the path does not exist, create it
    if not os.path.exists(root_path):
        os.makedirs(root_path)

    # Visualize the data
    if save:
        all_out_path = f"{root_path}/{lm.name}_{models[0]}_depth.png"
        visualize_data_axis_specific(out_path=all_out_path, data=plot_data, lm=lm, y_label="z in mm")

    if show:
        visualize_data_axis_specific(data=plot_data, lm=lm, y_label="z in mm", y_min=199, y_max=210, y_shift=1)


def show_2d_differences(lm=MPPoseLandmark.LEFT_ANKLE, show: bool = True, save: bool = True):
    baby = "NT"
    age = "27w"
    camera = "D435"  # User variable
    models = [MPModel.HEAVY, MPModel.FULL, MPModel.LITE, MPModel.VITPOSE]  # User variable
    axis = ['image_x', 'image_y']  # User variable
    base_gain = 1  # User variable

    _, camera_model_data_diff = read_camera_model_data([camera], models, baby, age)
    camera_model_axis = read_camera_model_axis([camera], models, axis, 1, -1, base_gain, camera_model_data_diff, lm)

    vitpose_x = np.array(camera_model_axis[camera][MPModel.VITPOSE]['image_x'])
    vitpose_y = np.array(camera_model_axis[camera][MPModel.VITPOSE]['image_y'])
    model_distances = {}
    for model in models:
        x = np.array(camera_model_axis[camera][model]['image_x'])
        y = np.array(camera_model_axis[camera][model]['image_y'])
        dist = np.sqrt(x ** 2 + y ** 2)
        model_distances[model] = dist

    # Compute differences from all models to VITPOSE
    models.remove(MPModel.VITPOSE)
    diff_distances = {}
    for model in models:
        x = np.array(camera_model_axis[camera][model]['image_x']) - vitpose_x
        y = np.array(camera_model_axis[camera][model]['image_y']) - vitpose_y
        dist = np.sqrt(x ** 2 + y ** 2)
        diff_distances[model] = dist

    colors = ['b', 'r', 'k', 'y', 'c', 'm']
    color_idx = 0
    # Prepare the data for plotting
    plot_data = []
    for model in models:
        plot_data.append(PlotData(data=diff_distances[model], label=f'{camera}_{model}', color=colors[color_idx]))
        color_idx += 1

    # Visualize also VITPOSE distances
    root_path = f"{get_root_path()}/output/{baby}/{age}/{camera}/images/{lm.name}"
    # if the path does not exist, create it
    if not os.path.exists(root_path):
        os.makedirs(root_path)

    if save:
        vitpose_out_path = f"{root_path}/{lm.name}_vitpose_2D_dist.png"
        visualize_data_axis_specific(out_path=vitpose_out_path,
                                     data=[
                                         PlotData(data=model_distances[MPModel.VITPOSE], label=f'{camera}_VITPOSE',
                                                  color='g')],
                                     lm=lm, y_label="Euclid Distance in Pixels")
    if show:
        visualize_data_axis_specific(
            data=[PlotData(data=model_distances[MPModel.VITPOSE], label=f'{camera}_VITPOSE', color='g')], lm=lm,
            y_label="Euclid Distance in Pixels")

    # Visualize the data
    if save:
        all_out_path = f"{root_path}/{lm.name}_2D_diff_from_vitpose.png"
        visualize_data_axis_specific(out_path=all_out_path, data=plot_data, lm=lm,
                                     y_label="Euclid Distance from Vitpose in Pixels")

    if show:
        visualize_data_axis_specific(data=plot_data, lm=lm, y_label="Euclid Distance from Vitpose in Pixels")


if __name__ == "__main__":
    show = True
    save = not show
    for lm in MPPoseLandmark:
        if lm.is_mocap():
            show_data_axis_specific(lm)
            # show_2d_differences(lm, show=show, save=save)
            # show_depth_differences(lm, show=show, save=save)
