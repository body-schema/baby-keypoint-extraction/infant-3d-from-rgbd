import os

import numpy as np

from src.detection.mediapipe_pose_detector import MPModel
from src.enums.mp_enum import MPPoseLandmark
from src.evaluation.eval import read_camera_model_axis, read_camera_model_data, get_mocap_data_specific, TIME_SHIFT_NT, \
    create_plotting_data, PlotData, visualize_data_axis_specific
from src.utils.path import get_root_path


def show_euclid_2d_diff(lm=MPPoseLandmark.LEFT_ANKLE, show: bool = True, save: bool = True, x_min=0, x_max=-1):
    axis = ['x', 'y']  # User variable
    base_gain = 1000  # User variable

    _, camera_model_data_diff = read_camera_model_data([camera], models, baby, age)
    camera_model_axis = read_camera_model_axis([camera], models, axis, 1, -1, base_gain, camera_model_data_diff, lm)

    # Add also Mocap data
    mocap_data_x = get_mocap_data_specific(baby, age, "good", lm, 'x', time_shift=TIME_SHIFT_NT)
    mocap_data_y = get_mocap_data_specific(baby, age, "good", lm, 'y', time_shift=TIME_SHIFT_NT)
    mocap_len = len(mocap_data_x)

    # Prepare the camera data for plotting
    model_distances = {}
    for model in models:
        x = np.array(camera_model_axis[camera][model]['x'])[:mocap_len] - np.array(
            mocap_data_y)  # Watch out for the axis
        y = np.array(camera_model_axis[camera][model]['y'])[:mocap_len] - np.array(
            mocap_data_x)  # Watch out for the axis
        dist = np.sqrt(x ** 2 + y ** 2)
        model_distances[model] = dist

    # make all data integers
    for model in models:
        model_distances[model] = model_distances[model].astype(int)

    # For each model, output its mean and std
    # for model in models:
    #     print(f"{model}: {np.mean(model_distances[model]):.2f} +/- {np.std(model_distances[model]):.2f}")

    colors = ['b', 'r', 'k', 'y', 'c', 'm']
    color_idx = 0
    # Prepare the data for plotting
    plot_data = []
    for model in models:
        plot_data.append(PlotData(data=model_distances[model], label=f'{camera}_{model}', color=colors[color_idx]))
        color_idx += 1

    root_path = f"{get_root_path()}/output/{baby}/{age}/{camera}/images/{lm.name}"
    # if the path does not exist, create it
    if not os.path.exists(root_path):
        os.makedirs(root_path)

    if x_max != -1:
        plot_data = [PlotData(data=d.data[x_min:x_max], label=d.label, color=d.color) for d in plot_data]

    # Visualize the data
    if save:
        all_out_path = f"{root_path}/{lm.name}_2D_diff_mm_from_Mocap.png"
        visualize_data_axis_specific(out_path=all_out_path, data=plot_data, lm=lm,
                                     y_label="Euclidean Distance in mm")

    if show:
        visualize_data_axis_specific(data=plot_data, lm=lm, y_label="Euclidean Distance in mm")


def show_euclid_2d_dist(lm=MPPoseLandmark.LEFT_ANKLE, show: bool = True, save: bool = True, x_min=0, x_max=-1):
    axis = ['x', 'y', 'image_x', 'image_y']  # User variable

    _, camera_model_data_diff = read_camera_model_data([camera], models, baby, age)
    camera_model_image_axis = read_camera_model_axis([camera], models, axis, 1, -1, 1, camera_model_data_diff, lm)
    camera_model_coord_axis = read_camera_model_axis([camera], models, axis, 1, -1, 1000, camera_model_data_diff, lm)

    # Prepare the camera data for plotting
    model_distances_image = {}
    model_distances_coord = {}
    for model in models:
        x = np.array(camera_model_coord_axis[camera][model]['x'])
        y = np.array(camera_model_coord_axis[camera][model]['y'])
        dist = np.sqrt(x ** 2 + y ** 2)
        model_distances_coord[model] = dist.astype(int)

        x = np.array(camera_model_image_axis[camera][model]['image_x'])
        y = np.array(camera_model_image_axis[camera][model]['image_y'])
        dist = np.sqrt(x ** 2 + y ** 2)
        model_distances_image[model] = dist.astype(int)

    # Add also Mocap data
    mocap_data_x = get_mocap_data_specific(baby, age, "good", lm, 'x', time_shift=TIME_SHIFT_NT)
    mocap_data_y = get_mocap_data_specific(baby, age, "good", lm, 'y', time_shift=TIME_SHIFT_NT)
    mocap_dist = np.sqrt(np.array(mocap_data_x) ** 2 + np.array(mocap_data_y) ** 2).astype(int)

    colors = ['b', 'r', 'k', 'y', 'c', 'm']
    color_idx = 0
    # Prepare the data for plotting
    plot_coord_data = []
    plot_image_data = []
    for model in models:
        plot_coord_data.append(
            PlotData(data=model_distances_coord[model], label=f'{camera}_{model}', color=colors[color_idx]))
        plot_image_data.append(
            PlotData(data=model_distances_image[model], label=f'{camera}_{model}', color=colors[color_idx]))
        color_idx += 1
    plot_coord_data.append(PlotData(data=mocap_dist, label=f'Mocap', color='g', marker='X'))

    # Cut all data to the same length as Mocap data
    min_len = min([len(d.data) for d in plot_coord_data])
    plot_coord_data = [PlotData(data=d.data[:min_len], label=d.label, color=d.color, marker=d.marker) for d in
                       plot_coord_data]
    plot_image_data = [PlotData(data=d.data[:min_len], label=d.label, color=d.color, marker=d.marker) for d in
                       plot_image_data]

    root_path = f"{get_root_path()}/output/{baby}/{age}/{camera}/images/{lm.name}"
    # if the path does not exist, create it
    if not os.path.exists(root_path):
        os.makedirs(root_path)

    if x_max != -1:
        plot_coord_data = [PlotData(data=d.data[x_min:x_max], label=d.label, color=d.color, marker=d.marker) for d in
                           plot_coord_data]
        plot_image_data = [PlotData(data=d.data[x_min:x_max], label=d.label, color=d.color, marker=d.marker) for d in
                           plot_image_data]

    # Visualize the data
    if save:
        all_out_path = f"{root_path}/{lm.name}_2D_dist_mm_from_origin.png"
        visualize_data_axis_specific(out_path=all_out_path, data=plot_coord_data, lm=lm,
                                     y_label="Euclidean Distance in mm")
        all_out_path = f"{root_path}/{lm.name}_2D_dist_pixel_from_origin.png"
        visualize_data_axis_specific(out_path=all_out_path, data=plot_image_data, lm=lm,
                                     y_label="Euclidean Distance in Pixels")

    if show:
        visualize_data_axis_specific(data=plot_coord_data, lm=lm, y_label="Euclidean Distance in mm")
        visualize_data_axis_specific(data=plot_image_data, lm=lm, y_label="Euclidean Distance in Pixels")


def shift_2d_to_mocap_and_show(lm, x_from, x_to, show: bool = True, save: bool = True):
    axis = ['x', 'y', 'image_x', 'image_y']  # User variable

    _, camera_model_data_diff = read_camera_model_data([camera], models, baby, age)
    camera_model_coord_axis = read_camera_model_axis([camera], models, axis, 1, -1, 1000, camera_model_data_diff, lm)

    # Prepare the camera data for plotting
    model_distances_diff = {}
    model_distances_dist = {}
    for model in models:
        x = np.array(camera_model_coord_axis[camera][model]['x'])
        y = np.array(camera_model_coord_axis[camera][model]['y'])
        dist = np.sqrt(x ** 2 + y ** 2)
        model_distances_dist[model] = dist

    # Add also Mocap data
    mocap_data_x = get_mocap_data_specific(baby, age, "good", lm, 'x', time_shift=TIME_SHIFT_NT)
    mocap_data_y = get_mocap_data_specific(baby, age, "good", lm, 'y', time_shift=TIME_SHIFT_NT)
    mocap_dist = np.sqrt(np.array(mocap_data_x) ** 2 + np.array(mocap_data_y) ** 2)
    mocap_len = len(mocap_dist)

    # Find the averaged offset on the interval and shift the data to the Mocap data
    for model in models:
        avg_off = np.mean(model_distances_dist[model][x_from:x_to] - mocap_dist[x_from:x_to])
        model_distances_dist[model] -= avg_off

    # Also compute the final diff
    for model in models:
        model_distances_diff[model] = model_distances_dist[model][:mocap_len] - mocap_dist

    # make all data integers
    for model in models:
        model_distances_dist[model] = model_distances_dist[model][:mocap_len].astype(int)
        model_distances_diff[model] = model_distances_diff[model].astype(int)
    mocap_dist = mocap_dist.astype(int)

    colors = ['b', 'r', 'k', 'y', 'c', 'm']
    color_idx = 0
    # Prepare the data for plotting
    plot_dist_data = []
    plot_diff_data = []
    for model in models:
        plot_dist_data.append(
            PlotData(data=model_distances_dist[model], label=f'{camera}_{model}', color=colors[color_idx]))
        plot_diff_data.append(
            PlotData(data=model_distances_diff[model], label=f'{camera}_{model}', color=colors[color_idx]))
        color_idx += 1
    plot_dist_data.append(PlotData(data=mocap_dist, label=f'Mocap', color='g', marker='X'))

    # Cut all data to the same length as Mocap data
    min_len = min([len(d.data) for d in plot_dist_data])
    plot_dist_data = [PlotData(data=d.data[:min_len], label=d.label, color=d.color, marker=d.marker) for d in
                      plot_dist_data]
    plot_diff_data = [PlotData(data=d.data[:min_len], label=d.label, color=d.color, marker=d.marker) for d in
                      plot_diff_data]

    root_path = f"{get_root_path()}/output/{baby}/{age}/{camera}/images/{lm.name}"
    # if the path does not exist, create it
    if not os.path.exists(root_path):
        os.makedirs(root_path)

    # Visualize the data
    if save:
        all_out_path = f"{root_path}/{lm.name}_2D_dist_mm_shifted_to_mocap.png"
        visualize_data_axis_specific(out_path=all_out_path, data=plot_dist_data, lm=lm,
                                     y_label="Euclidean Distance in mm")
        all_out_path = f"{root_path}/{lm.name}_2D_diff_mm_shifted_to_mocap.png"
        visualize_data_axis_specific(out_path=all_out_path, data=plot_diff_data, lm=lm,
                                     y_label="Euclidean Distance in mm")

    if show:
        visualize_data_axis_specific(data=plot_dist_data, lm=lm, y_label="Euclidean Distance in mm")
        visualize_data_axis_specific(data=plot_diff_data, lm=lm, y_label="Euclidean Distance in mm")


def output_measure_info():
    cameras = ["D435", "D455", "ZED"]  # User variable
    axis = ['x', 'y']  # User variable
    _, camera_model_data_diff = read_camera_model_data(cameras, models, baby, age)
    for lm in MPPoseLandmark:
        if lm.is_mocap():
            camera_model_axis = read_camera_model_axis(cameras, models, axis, 1, -1, 1000, camera_model_data_diff,
                                                       lm)
            # Add also Mocap data
            mocap_data_x = get_mocap_data_specific(baby, age, "good", lm, 'x', time_shift=TIME_SHIFT_NT)
            mocap_data_y = get_mocap_data_specific(baby, age, "good", lm, 'y', time_shift=TIME_SHIFT_NT)
            mocap_len = len(mocap_data_x)

            # Prepare the camera data for plotting
            model_distances = {}
            for cam in cameras:
                model_distances[cam] = {}
                for model in models:
                    x = np.array(camera_model_axis[cam][model]['x'])[:mocap_len] - np.array(
                        mocap_data_y)  # Watch out for the axis
                    y = np.array(camera_model_axis[cam][model]['y'])[:mocap_len] - np.array(
                        mocap_data_x)  # Watch out for the axis
                    dist = np.sqrt(x ** 2 + y ** 2)
                    model_distances[cam][model] = dist

            # make all data integers
            for cam in cameras:
                for model in models:
                    model_distances[cam][model] = model_distances[cam][model].astype(int)

            print(f"Landmark: {lm.name}")
            # For each model, output its mean and std
            for cam in cameras:
                print(f"Camera: {cam}")
                for model in models:
                    print(
                        f"{model}: {np.mean(model_distances[cam][model]):.2f} +/- {np.std(model_distances[cam][model]):.2f}")


if __name__ == "__main__":
    baby = "NT"
    age = "27w"
    camera = "D435"  # User variable
    models = [MPModel.HEAVY, MPModel.FULL, MPModel.LITE, MPModel.VITPOSE]  # User variable

    show = True
    save = not show

    # for lm in MPPoseLandmark:
    #     if lm.is_mocap():
    #         show_euclid_2d_dist(lm, show, save)
    #         show_euclid_2d_diff(lm, show, save)

    # x_min = 300
    # x_max = 475
    # lm = MPPoseLandmark.RIGHT_HIP
    # show_euclid_2d_dist(lm, show, save)
    # show_euclid_2d_diff(lm, show, save)
    # shift_to_mocap_and_show(lm, x_min, x_max, show, save)

    output_measure_info()
