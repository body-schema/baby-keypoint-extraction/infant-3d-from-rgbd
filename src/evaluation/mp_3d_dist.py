import os

import numpy as np

from src.detection.mediapipe_pose_detector import MPModel
from src.enums.mp_enum import MPPoseLandmark
from src.evaluation.eval import read_camera_model_data, read_camera_model_axis, create_plotting_data, \
    get_mocap_data_specific, TIME_SHIFT_NT, PlotData, visualize_data_axis_specific
from src.utils.path import get_root_path


def normalize(data):
    min_val = np.min(data)
    max_val = np.max(data)
    return (data - min_val) / (max_val - min_val)


def show_euclid_mp_3d_dist(lm=MPPoseLandmark.LEFT_ANKLE, show: bool = True, save: bool = True, x_min=0, x_max=-1):
    axis = ['model_x', 'model_y', 'model_z']  # User variable

    _, camera_model_data_diff = read_camera_model_data(cameras, models, baby, age)
    camera_model_coord_axis = read_camera_model_axis(cameras, models, axis, 1, -1, 1000, camera_model_data_diff, lm)

    # Prepare the camera data for plotting
    model_distances_coord = {}
    camera = cameras[0]
    for model in models:
        x = np.array(camera_model_coord_axis[camera][model]['model_x'])
        y = np.array(camera_model_coord_axis[camera][model]['model_y'])
        z = np.array(camera_model_coord_axis[camera][model]['model_z'])
        dist = np.sqrt(x ** 2 + y ** 2)
        model_distances_coord[model] = dist

    # Add also Mocap data
    mocap_data_x = get_mocap_data_specific(baby, age, "good", lm, 'x', time_shift=TIME_SHIFT_NT)
    mocap_data_y = get_mocap_data_specific(baby, age, "good", lm, 'y', time_shift=TIME_SHIFT_NT)
    mocap_data_z = get_mocap_data_specific(baby, age, "good", lm, 'z', time_shift=TIME_SHIFT_NT)
    mocap_dist = np.sqrt(np.array(mocap_data_x) ** 2 + np.array(mocap_data_y) ** 2)
    # mocap_dist = normalize(mocap_dist)
    # mocap_data_min = np.min(mocap_dist)
    # mocap_data_max = np.max(mocap_dist)
    # mocap_dist = (mocap_dist - mocap_data_min) / (mocap_data_max - mocap_data_min)

    # make all data integers
    for model in models:
        model_distances_coord[model] = normalize(model_distances_coord[model])
    mocap_dist = mocap_dist

    colors = ['b', 'r', 'k', 'y', 'c', 'm']
    color_idx = 0
    # Prepare the data for plotting
    plot_coord_data = []
    for model in models:
        plot_coord_data.append(
            PlotData(data=model_distances_coord[model], label=f'{camera}_{model}', color=colors[color_idx]))
        color_idx += 1
    plot_coord_data.append(PlotData(data=mocap_dist, label=f'Mocap', color='g', marker='X'))

    # Cut all data to the same length as Mocap data
    min_len = min([len(d.data) for d in plot_coord_data])
    plot_coord_data = [PlotData(data=d.data[:min_len], label=d.label, color=d.color, marker=d.marker) for d in
                       plot_coord_data]

    if x_max != -1:
        plot_coord_data = [PlotData(data=d.data[x_min:x_max], label=d.label, color=d.color, marker=d.marker) for d in
                           plot_coord_data]

    visualize_data_axis_specific(data=plot_coord_data, lm=lm, y_label="Euclidean Distance in mm")


def show_mp_3d_dist(lm=MPPoseLandmark.LEFT_ANKLE, show: bool = True, save: bool = True, x_min=0, x_max=-1):
    axis = ['model_z']  # User variable

    _, camera_model_data_diff = read_camera_model_data(cameras, models, baby, age)
    camera_model_axis = read_camera_model_axis(cameras, models, axis, 1, -1, 1000, camera_model_data_diff, lm)

    model = models[0]
    cam = cameras[0]
    a = axis[0]
    data = camera_model_axis[cam][model][a]
    plot_data = [PlotData(data=data, label=f'{cam}_{model}_{a}', color='b')]

    # normalize the data
    data_min = np.min(data)
    data_max = np.max(data)
    normalized_data = (data - data_min) / (data_max - data_min)
    normalized_plot_data = [PlotData(data=normalized_data, label=f'{cam}_{model}_{a}', color='b')]

    # Add also Mocap data
    mocap_data = get_mocap_data_specific(baby, age, "good", lm, 'z', time_shift=TIME_SHIFT_NT)
    plot_data.append(PlotData(data=mocap_data, label=f'Mocap', color='g', marker='X'))
    mocap_data_min = np.min(mocap_data)
    mocap_data_max = np.max(mocap_data)
    normalized_mocap_data = (mocap_data - mocap_data_min) / (mocap_data_max - mocap_data_min)
    normalized_plot_data.append(PlotData(data=normalized_mocap_data, label=f'Mocap', color='g', marker='X'))

    # Cut all data to the same length as Mocap data
    min_len = min([len(d.data) for d in normalized_plot_data])
    normalized_plot_data = [PlotData(data=d.data[:min_len], label=d.label, color=d.color, marker=d.marker) for d in
                            normalized_plot_data]
    plot_data = [PlotData(data=d.data[:min_len], label=d.label, color=d.color, marker=d.marker) for d in plot_data]

    # Visualize the raw data
    visualize_data_axis_specific(data=plot_data, lm=lm, y_label=f'Z in mm')
    # Visualize the normalized data
    visualize_data_axis_specific(data=normalized_plot_data, lm=lm, y_label=f'Normalized Z')


if __name__ == "__main__":
    baby = "NT"
    age = "27w"
    cameras = ["D435"]  # User variable
    models = [MPModel.HEAVY]  # User variable

    show = True
    save = not show

    for lm in MPPoseLandmark:
        if lm.is_mocap():
            # show_euclid_mp_3d_dist(lm, show, save)
            show_mp_3d_dist(lm, show, save)
