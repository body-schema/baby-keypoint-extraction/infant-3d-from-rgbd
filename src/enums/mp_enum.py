from enum import Enum


class MPPoseLandmark(Enum):
    NOSE = 0
    LEFT_EYE_INNER = 1
    LEFT_EYE = 2
    LEFT_EYE_OUTER = 3
    RIGHT_EYE_INNER = 4
    RIGHT_EYE = 5
    RIGHT_EYE_OUTER = 6
    LEFT_EAR = 7
    RIGHT_EAR = 8
    MOUTH_LEFT = 9
    MOUTH_RIGHT = 10
    LEFT_SHOULDER = 11
    RIGHT_SHOULDER = 12
    LEFT_ELBOW = 13
    RIGHT_ELBOW = 14
    LEFT_WRIST = 15
    RIGHT_WRIST = 16
    LEFT_PINKY = 17
    RIGHT_PINKY = 18
    LEFT_INDEX = 19
    RIGHT_INDEX = 20
    LEFT_THUMB = 21
    RIGHT_THUMB = 22
    LEFT_HIP = 23
    RIGHT_HIP = 24
    LEFT_KNEE = 25
    RIGHT_KNEE = 26
    LEFT_ANKLE = 27
    RIGHT_ANKLE = 28
    LEFT_HEEL = 29
    RIGHT_HEEL = 30
    LEFT_FOOT_INDEX = 31
    RIGHT_FOOT_INDEX = 32

    def is_coco(self):
        if self == MPPoseLandmark.NOSE or self == MPPoseLandmark.LEFT_EYE or self == MPPoseLandmark.RIGHT_EYE or \
                self == MPPoseLandmark.LEFT_EAR or self == MPPoseLandmark.RIGHT_EAR or \
                self == MPPoseLandmark.LEFT_SHOULDER or self == MPPoseLandmark.RIGHT_SHOULDER or \
                self == MPPoseLandmark.LEFT_ELBOW or self == MPPoseLandmark.RIGHT_ELBOW or \
                self == MPPoseLandmark.LEFT_WRIST or self == MPPoseLandmark.RIGHT_WRIST or \
                self == MPPoseLandmark.LEFT_HIP or self == MPPoseLandmark.RIGHT_HIP or \
                self == MPPoseLandmark.LEFT_KNEE or self == MPPoseLandmark.RIGHT_KNEE or \
                self == MPPoseLandmark.LEFT_ANKLE or self == MPPoseLandmark.RIGHT_ANKLE:
            return True
        return False

    def is_mocap(self):
        if self == MPPoseLandmark.RIGHT_SHOULDER or self == MPPoseLandmark.RIGHT_ELBOW or \
                self == MPPoseLandmark.RIGHT_WRIST or self == MPPoseLandmark.RIGHT_HIP or \
                self == MPPoseLandmark.RIGHT_KNEE or self == MPPoseLandmark.RIGHT_ANKLE or \
                self == MPPoseLandmark.LEFT_SHOULDER or self == MPPoseLandmark.LEFT_ELBOW or \
                self == MPPoseLandmark.LEFT_WRIST or self == MPPoseLandmark.LEFT_HIP or \
                self == MPPoseLandmark.LEFT_KNEE or self == MPPoseLandmark.LEFT_ANKLE:
            return True
        return False

    def to_mocap(self):
        if self == MPPoseLandmark.RIGHT_SHOULDER:
            return "r_shoulder"
        if self == MPPoseLandmark.RIGHT_ELBOW:
            return "r_elbow"
        if self == MPPoseLandmark.RIGHT_WRIST:
            return "r_wrist"
        if self == MPPoseLandmark.RIGHT_HIP:
            return "r_hip"
        if self == MPPoseLandmark.RIGHT_KNEE:
            return "r_knee"
        if self == MPPoseLandmark.RIGHT_ANKLE:
            return "r_ankle"
        if self == MPPoseLandmark.LEFT_SHOULDER:
            return "l_shoulder"
        if self == MPPoseLandmark.LEFT_ELBOW:
            return "l_elbow"
        if self == MPPoseLandmark.LEFT_WRIST:
            return "l_wrist"
        if self == MPPoseLandmark.LEFT_HIP:
            return "l_hip"
        if self == MPPoseLandmark.LEFT_KNEE:
            return "l_knee"
        if self == MPPoseLandmark.LEFT_ANKLE:
            return "l_ankle"
