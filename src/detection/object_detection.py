import cv2
import mediapipe as mp
from mediapipe.tasks import python
from mediapipe.tasks.python import vision

from src.utils.path import get_root_path


def find_human_in_image(image_path, model_path, out_path):
    BaseOptions = mp.tasks.BaseOptions
    ObjectDetector = mp.tasks.vision.ObjectDetector
    ObjectDetectorOptions = mp.tasks.vision.ObjectDetectorOptions
    VisionRunningMode = mp.tasks.vision.RunningMode

    options = ObjectDetectorOptions(
        base_options=BaseOptions(model_asset_path=model_path),
        max_results=5,
        running_mode=VisionRunningMode.IMAGE)

    with ObjectDetector.create_from_options(options) as detector:
        # Load the input image from an image file.
        mp_image = mp.Image.create_from_file(image_path)

        # Load the input image from a numpy array.
        # mp_image = mp.Image(image_format=mp.ImageFormat.SRGB, data=numpy_image)

        # Perform object detection on the provided single image.
        detection_result = detector.detect(mp_image)

        # Process the detection result.
        for detection in detection_result.detections:
            # show person bounding boxes
            height = detection.bounding_box.height
            width = detection.bounding_box.width
            origin_x = detection.bounding_box.origin_x
            origin_y = detection.bounding_box.origin_y
            if detection.categories[0].category_name == "person":
                print(f"Detected person at: ({origin_x}, {origin_y}), "
                      f"width: {width}, height: {height}")
                # show rectangle
                image = cv2.imread(image_path)
                cv2.rectangle(image, (origin_x, origin_y),
                              (origin_x + width, origin_y + height),
                              (0, 255, 0), 2)
                # show in the middle the pixel density of rectangle
                cv2.putText(image, f"{width}x{height}",
                            (origin_x, origin_y - 10),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 255, 0), 2)

                cv2.imshow("Person Detection", image)
                # store image with bounding box
                cv2.imwrite(out_path, image)
                break


if __name__ == "__main__":
    img = f"{get_root_path()}/resources/NT/27w/D435_1.png"
    model_path = f"{get_root_path()}/models/efficientdet_lite0.tflite"
    out = f"{get_root_path()}/resources/NT/27w/D435_1_bbx.png"
    find_human_in_image(img, model_path, out)
