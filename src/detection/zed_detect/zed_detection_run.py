import pyzed.sl as sl
import cv2
import numpy as np


def main():
    # Create a Camera object
    zed = sl.Camera()

    # Create a InitParameters object and set configuration parameters
    init_params = sl.InitParameters()
    init_params.camera_resolution = sl.RESOLUTION.HD720  # Use HD720 video mode
    init_params.depth_mode = sl.DEPTH_MODE.PERFORMANCE
    init_params.coordinate_units = sl.UNIT.METER
    init_params.sdk_verbose = 1

    # Open the camera
    err = zed.open(init_params)
    if err != sl.ERROR_CODE.SUCCESS:
        print("Camera Open : " + repr(err) + ". Exit program.")
        exit()

    body_params = sl.BodyTrackingParameters()
    # Different model can be chosen, optimizing the runtime or the accuracy
    body_params.detection_model = sl.BODY_TRACKING_MODEL.HUMAN_BODY_FAST
    body_params.enable_tracking = True
    body_params.image_sync = True
    body_params.enable_segmentation = False
    # Optimize the person joints position, requires more computations
    body_params.enable_body_fitting = True
    body_params.body_format = sl.BODY_FORMAT.BODY_18

    if body_params.enable_tracking:
        positional_tracking_param = sl.PositionalTrackingParameters()
        # positional_tracking_param.set_as_static = True
        positional_tracking_param.set_floor_as_origin = True
        zed.enable_positional_tracking(positional_tracking_param)

    print("Body tracking: Loading Module...")

    # Set runtime parameters
    runtime_parameters = sl.RuntimeParameters()

    err = zed.enable_body_tracking(body_params)
    if err != sl.ERROR_CODE.SUCCESS:
        print("Enable Body Tracking : " + repr(err) + ". Exit program.")
        zed.close()
        exit()

    bodies = sl.Bodies()
    image_rgb = sl.Mat()
    body_runtime_param = sl.BodyTrackingRuntimeParameters()
    # For outdoor scene or long range, the confidence should be lowered to avoid missing detections (~20-30)
    # For indoor scene or closer range, a higher confidence limits the risk of false positives and increase the precision (~50+)
    body_runtime_param.detection_confidence_threshold = 40
    while True:
        if zed.grab(runtime_parameters) == sl.ERROR_CODE.SUCCESS:
            # Retrieve the left image in sl.Mat
            zed.retrieve_image(image_rgb, sl.VIEW.LEFT)
            # Retrieve detected bodies
            zed.retrieve_bodies(bodies, body_runtime_param)
            # Convert sl.Mat to cv2 image (numpy array)
            image_ocv = image_rgb.get_data()

            people_detected = len(bodies.body_list)
            # Put the number of detected people on the image
            cv2.putText(image_ocv, "Detected : " + str(people_detected), (20, 70), cv2.FONT_HERSHEY_SIMPLEX, 1,
                        (0, 255, 0), 2)

            # Draw the keypoints on the image
            for body in bodies.body_list:
                position = body.position
                velocity = body.velocity
                dimensions = body.dimensions
                for keypoint in body.keypoint_2d:  # 2D keypoints
                    cv2.circle(image_ocv, (int(keypoint[0]), int(keypoint[1])), 5, (0, 255, 0), -1)
                for keypoint in body.keypoint:  # 3D keypoints
                    continue

            # Display image with keypoints
            cv2.imshow("ZED | Body Tracking", image_ocv)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    # Close the camera
    zed.close()


if __name__ == "__main__":
    main()
