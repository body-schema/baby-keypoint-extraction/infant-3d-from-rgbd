import cv2

from src.enums.zed_enum import ZedPoseLandmark
from src.utils.csv_writer import CSVWriter
from src.enums.defaults import DEFAULT_FPS, ZED_DEFAULT_WIDTH, \
    ZED_DEFAULT_HEIGHT
from src.streaming.cameras.zed import ZedCamera
from src.utils.path import get_root_path
from src.utils.video_writer import VideoWriter


class ZedBodyTracking:
    def __init__(self, svo_file: str = None, save: bool = False):
        self.camera = ZedCamera(svo_file, save, body_tracking=True, flip=True)

    def run(self, output_video_file: str, output_csv_file: str, coco_on: bool = True, svo_from: int = 0):
        # Neither output file cannot be None
        assert output_video_file is not None and output_csv_file is not None, "Output files cannot be None."
        video_writer = VideoWriter(output_video_file, DEFAULT_FPS, (ZED_DEFAULT_WIDTH, ZED_DEFAULT_HEIGHT))
        csv_writer = CSVWriter(output_csv_file)

        idx: int = 0
        is_paused = False  # Variable to check if the video is paused or not

        self.camera.set_svo_position(svo_from)
        while True:
            key_pressed = cv2.waitKey(1) & 0xFF
            # if Space is pressed, toggle the pause state
            if key_pressed == ord(' '):
                is_paused = not is_paused
            # if Q is pressed, exit the loop
            elif key_pressed == ord('q'):
                break

            # Only grab the next frame if the video is not paused
            # if N is pressed and the video is paused, grab the next frame
            if is_paused and not key_pressed == ord('n'):
                continue

            idx += 1
            self.camera.run()

            annotated_image = self.camera.get_rgb_img().copy()

            frame_number = self.camera.get_frame_number()
            bodies = self.camera.get_bodies()
            timestamp = self.camera.get_timestamp()
            self.camera.draw_keypoints(annotated_image, bodies)

            written: bool = False
            for body in bodies:
                for landmark_id, (landmark_xyz, pixel_coordinates) in enumerate(zip(body.keypoint, body.keypoint_2d)):

                    zed_pose_lm = ZedPoseLandmark(landmark_id)
                    if (coco_on and zed_pose_lm.is_coco()) or not coco_on:
                        world_coordinates = self.camera.deproject_pixel(pixel_coordinates)

                        # show pixel coordinates on the image with red
                        # TODO: Both XY and XYZ coordinates will be LATER transposed to calibrated ORIGIN

                        csv_writer.write(idx, frame_number, timestamp, landmark_xyz, pixel_coordinates,
                                         world_coordinates, zed_pose_lm)
                        written = True

                break  # Only one body is tracked

            if not written:  # If no landmarks detected, write an empty landmark
                for empty_lm in ZedPoseLandmark:
                    if (coco_on and empty_lm.is_coco()) or not coco_on:
                        csv_writer.write(idx, frame_number, timestamp, (0, 0, 0), (0, 0), (0, 0, 0), empty_lm)
                print("No landmarks detected.")

            # Write the annotated image to the video file
            video_writer.write_frame(annotated_image)

            # Put some info on the image
            self.camera.put_info_to_image(annotated_image, idx)

            cv2.imshow("Annotated Image", annotated_image)

        self.camera.close()
        video_writer.close()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    camera_name = "ZED"
    baby = "NT"
    age = "27w"
    parent_folder = f"resources/{baby}/{age}/rgbd"
    filename = f"{camera_name}"
    svo_file = f"{get_root_path()}/{parent_folder}/{filename}.svo"

    zed_body_tracking = ZedBodyTracking(svo_file, save=False)
    video_file = f"{get_root_path()}/output/{baby}/{age}/{camera_name}/native/{filename}_native.mp4"
    csv_file = f"{get_root_path()}/output/{baby}/{age}/{camera_name}/native/{filename}_native.csv"

    is_coco = True
    zed_body_tracking.run(video_file, csv_file, is_coco, svo_from=8000)
