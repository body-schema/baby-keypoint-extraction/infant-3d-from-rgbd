import cv2
import numpy as np
from mediapipe.tasks.python.components.containers.landmark import Landmark, NormalizedLandmark

from src.streaming.cameras.realsense import RealsenseCamera
from src.streaming.cameras.zed import ZedCamera
from src.detection.mediapipe_pose_detector import MPModel, MPPoseDetector
from src.enums.mp_enum import MPPoseLandmark
from src.utils.csv_writer import CSVWriter
from src.utils.path import get_root_path
from src.utils.plot_utils import rotate_dim, rotate_img
from src.utils.video_writer import VideoWriter


class MediapipeOnline:
    def __init__(self, camera_name: str, input_file: str = None, save: bool = False, model: MPModel = MPModel.FULL):
        # If input file is .svo file, use ZedCamera
        if camera_name == "zed" or input_file.endswith(".svo"):
            self.camera = ZedCamera(input_file, save, body_tracking=False, flip=True)
        # If input file is .bag file, use RealsenseCamera
        elif camera_name == "D435" or camera_name == "D455" or input_file.endswith(".bag"):
            self.camera = RealsenseCamera(input_file, save)
        else:
            raise ValueError(f"Invalid input args for MP runner.")
        self._pose_detector = MPPoseDetector(model)

    def run(self, output_video_file: str, output_csv_file: str, coco_on: bool = True):
        # Neither output file cannot be None
        assert output_video_file is not None and output_csv_file is not None, "Output files cannot be None."
        frame_dim = self.camera.get_defaults_dimensions()
        video_writer = VideoWriter(output_video_file, self.camera.get_fps(), frame_dim)
        csv_writer = CSVWriter(output_csv_file)

        roi = None
        roi_color_image = None

        rotation_angle: int = 0
        idx: int = 0
        is_paused = False  # Variable to check if the video is paused or not
        last_timestamp = 0

        # self.camera.set_svo_position(7900)
        while True:
            key_pressed = cv2.waitKey(1) & 0xFF
            # if Space is pressed, toggle the pause state
            if key_pressed == ord(' '):
                is_paused = not is_paused
            # if Q is pressed, exit the loop
            elif key_pressed == ord('q'):
                break
            # if R is pressed, rotate the image
            elif key_pressed == ord('r') and roi is None:
                # TODO: Not working properly. Problem with ZED camera.
                rotation_angle += 90
                rotation_angle %= 360
                # Reinitialize the video writer with the rotated dimensions
                frame_dim = rotate_dim(frame_dim, rotation_angle)
                video_writer = VideoWriter(output_video_file, self.camera.get_fps(), frame_dim)
                # Reinitialize the CSV writer
                csv_writer = CSVWriter(output_csv_file)

            # Only grab the next frame if the video is not paused
            # if N is pressed and the video is paused, grab the next frame
            if is_paused and not key_pressed == ord('n'):
                continue

            idx += 1
            self.camera.run()

            color_image = self.camera.get_rgb_img().copy()
            color_image = rotate_img(color_image, rotation_angle)
            annotated_image = color_image.copy()

            depth_image = self.camera.get_depth_img().copy()
            depth_image = rotate_img(depth_image, rotation_angle)

            frame_number = self.camera.get_frame_number()
            timestamp = int(self.camera.get_timestamp())
            if last_timestamp > timestamp:
                raise ValueError(f"Timestamps are not in order: {last_timestamp} > {timestamp}. Probably EOF.")
            last_timestamp = timestamp

            # if X is pressed, take the ROI
            if key_pressed == ord('x'):
                # Crop both images with ROI (Region of Interest)
                roi = cv2.selectROI("Color Image", color_image)
                # Reinitialize the video writer
                video_writer = VideoWriter(output_video_file, self.camera.get_fps(), frame_dim)
                # Reinitialize the CSV writer
                csv_writer = CSVWriter(output_csv_file)

            if roi is not None:
                x, y, w, h = roi
                roi_color_image = color_image[y:y + h, x:x + w, :].astype(np.uint8)

                pose_landmarks = self._pose_detector.detect_pose(roi_color_image, timestamp)
                roi_annotated_image = MPPoseDetector.draw_landmarks_on_image(roi_color_image, pose_landmarks)
                cv2.imshow("ROI Annotated Image", roi_annotated_image)
            else:
                pose_landmarks = self._pose_detector.detect_pose(color_image, timestamp)
                annotated_image = MPPoseDetector.draw_landmarks_on_image(color_image, pose_landmarks)

            written: bool = False
            for pose_world_landmarks, normalized_landmarks in zip(pose_landmarks.pose_world_landmarks,
                                                                  pose_landmarks.pose_landmarks):
                for landmark_id, (landmark_xyz, landmark_xy) in enumerate(
                        zip(pose_world_landmarks, normalized_landmarks)):

                    mp_pose_lm = MPPoseLandmark(landmark_id)
                    if (coco_on and mp_pose_lm.is_coco()) or not coco_on:
                        # Translate the normalized landmark to the image coordinate system
                        if roi is not None:
                            landmark_xy.x = int(landmark_xy.x * roi_color_image.shape[1]) + x
                            landmark_xy.y = int(landmark_xy.y * roi_color_image.shape[0]) + y
                            # show the pixels on the image
                            cv2.circle(annotated_image, (landmark_xy.x, landmark_xy.y), 4, (0, 255, 0), -1)
                        else:
                            landmark_xy.x = int(landmark_xy.x * color_image.shape[1])
                            landmark_xy.y = int(landmark_xy.y * color_image.shape[0])

                        pixel_coordinates = np.array([landmark_xy.x, landmark_xy.y])
                        world_coordinates = self.camera.deproject_pixel(pixel_coordinates, depth_image)

                        # TODO: Both XY and XYZ coordinates will be LATER transposed to calibrated ORIGIN

                        csv_writer.write(idx, frame_number, timestamp, landmark_xyz, landmark_xy, world_coordinates,
                                         mp_pose_lm)
                        written = True

            if not written:  # If no landmarks detected, write an empty landmark
                empty_landmark_xyz = Landmark(0, 0, 0, 0, 0)
                empty_landmark_xy = NormalizedLandmark(0, 0, 0)
                world_coordinates = (0, 0, 0)
                for empty_lm in MPPoseLandmark:
                    if (coco_on and empty_lm.is_coco()) or not coco_on:
                        csv_writer.write(idx, frame_number, timestamp, empty_landmark_xyz, empty_landmark_xy,
                                         world_coordinates, empty_lm)
                # print("No landmarks detected.")

            # Write the annotated image to the video file
            video_writer.write_frame(annotated_image)

            # Put some info on the image
            self.camera.put_info_to_image(annotated_image, idx)

            cv2.imshow("Annotated Image", annotated_image)

        self.camera.close()
        video_writer.close()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    # camera_name = "D435"
    camera_name = "D455"
    # camera_name = "ZED"

    baby = "NT"
    age = "27w"
    parent_folder = f"resources/{baby}/{age}/rgbd"
    filename = f"{camera_name}"

    ends = ".bag" if camera_name == "D455" or camera_name == "D435" else ".svo"
    input_file = f"{get_root_path()}/{parent_folder}/{filename}{ends}"
    mp_model = MPModel.LITE
    realsense_mp = MediapipeOnline(camera_name, input_file, save=False, model=mp_model)

    video_file = f"{get_root_path()}/output/{baby}/{age}/{camera_name}/{mp_model}/{filename}_{mp_model}.mp4"
    csv_file = f"{get_root_path()}/output/{baby}/{age}/{camera_name}/{mp_model}/{filename}_{mp_model}.csv"
    realsense_mp.run(video_file, csv_file)
