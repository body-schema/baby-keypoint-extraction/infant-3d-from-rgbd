import cv2
import numpy as np
import pandas as pd
import pyrealsense2 as rs

from src.enums.coco_enum import CoCo
from src.streaming.cameras.zed import ZedCamera
from src.utils.csv_writer import CSVWriter
from src.utils.path import get_root_path
from src.utils.plot_utils import rotate_img
from src.utils.video_writer import ColorDepthReader, ColorReader


class ViTPoseOffline:
    def __init__(self, vitpose_csv: str, reference_video_file: str, output_csv_file: str):
        self._color_stream = ColorReader(reference_video_file)
        self._vitpose_df = pd.read_csv(vitpose_csv)
        self._csv_writer = CSVWriter(output_csv_file)

    def run_with_zed(self, svo_file: str, idx_from: int, idx_to: int):
        camera = ZedCamera(svo_file, save=False, body_tracking=False, flip=True)
        frame_idx: int = 1

        while True:
            key_pressed = cv2.waitKey(1) & 0xFF
            # if Q is pressed, exit the loop
            if key_pressed == ord('q'):
                break

            camera.run()
            color_image = camera.get_rgb_img().copy()
            depth_image = camera.get_depth_img().copy()

            svo_idx = camera.get_frame_number()
            if svo_idx < idx_from:
                cv2.imshow("Color Image", color_image)
                svo_idx += 1
                continue
            elif svo_idx > idx_to:
                break

            ret_color, reference_color_image = self._color_stream.get_frames()
            annotated_image = color_image.copy()

            # Get the landmarks from the ViTPose CSV file
            lms = self._vitpose_df.loc[self._vitpose_df['frame_idx'] == frame_idx]
            # If number of rows is greater than amount of CoCo landmarks, then use only the first 17 rows
            if len(lms) > len(CoCo):
                lms = lms[:len(CoCo)]
            # Iterate through each row in lms and deproject the pixel coordinates to world coordinates
            for _, row in lms.iterrows():
                lm = CoCo(row['landmark_id'])
                pixel_coordinates = np.array([row['image_x'], row['image_y']])

                # Show pixel coordinates in annotated image
                cv2.circle(annotated_image, tuple(pixel_coordinates), 5, (0, 255, 0), -1)

                # TODO: Watch out, the ViTPose pixels might be differently rotated.

                world_coordinates = camera.deproject_pixel(pixel_coordinates, depth_image)
                self._csv_writer.write(frame_idx, frame_idx, frame_idx, (0, 0, 0),
                                       pixel_coordinates, world_coordinates, lm)

            cv2.imshow("Annotated Image", annotated_image)
            cv2.imshow("Reference Image", reference_color_image)
            frame_idx += 1

        cv2.destroyAllWindows()
        camera.close()
        self._color_stream.close()

    def run_with_rs(self, stream_root: str, rotation_angle: int = 0):
        stream_reader = ColorDepthReader(stream_root)
        frame_idx: int = 1

        while True:
            key_pressed = cv2.waitKey(1) & 0xFF
            # if Q is pressed, exit the loop
            if key_pressed == ord('q'):
                break

            ret_color, reference_color_image = self._color_stream.get_frames()
            ret_color, color_image, ret_depth, depth_image = stream_reader.get_frames()

            if not ret_color or not ret_depth:
                print("No more frames to read.")
                break

            reference_color_image = rotate_img(reference_color_image, rotation_angle)
            color_image = rotate_img(color_image, rotation_angle)
            depth_image = rotate_img(depth_image, rotation_angle)
            annotated_image = color_image.copy()

            # Get the landmarks from the ViTPose CSV file
            lms = self._vitpose_df.loc[self._vitpose_df['frame_idx'] == frame_idx]
            # If number of rows is greater than amount of CoCo landmarks, then use only the first 17 rows
            if len(lms) > len(CoCo):
                lms = lms[:len(CoCo)]
            # Iterate through each row in lms and deproject the pixel coordinates to world coordinates
            for _, row in lms.iterrows():
                lm = CoCo(row['landmark_id'])
                pixel_coordinates = np.array([row['image_x'], row['image_y']])

                # Show pixel coordinates in annotated image
                cv2.circle(annotated_image, tuple(pixel_coordinates), 5, (0, 255, 0), -1)

                # TODO: Watch out, the ViTPose pixels might be differently rotated.

                depth = depth_image[pixel_coordinates[1], pixel_coordinates[0]] / 1000
                world_coordinates = rs.rs2_deproject_pixel_to_point(stream_reader.intrinsics,
                                                                    pixel_coordinates, depth)

                self._csv_writer.write(frame_idx, frame_idx, frame_idx, (0, 0, 0), pixel_coordinates, world_coordinates,
                                       lm)

            cv2.imshow("Annotated Image", annotated_image)
            cv2.imshow("Reference Image", reference_color_image)
            frame_idx += 1

        cv2.destroyAllWindows()
        stream_reader.close()
        self._color_stream.close()


if __name__ == "__main__":
    # camera_name = "D435"
    # camera_name = "D455"
    camera_name = "ZED"

    baby = "NT"
    age = "27w"
    parent_folder = f"{get_root_path()}/output/{baby}/{age}/{camera_name}/"

    reference_stream = f"{parent_folder}/vitpose/vis_{camera_name}_short_color.mp4"
    stream_root = f"{parent_folder}/{camera_name}_short"
    input_csv_file = f"{parent_folder}/vitpose/vitpose.csv"
    output_csv_file = f"{parent_folder}/vitpose/vitpose_out.csv"

    vitpose = ViTPoseOffline(input_csv_file, reference_stream, output_csv_file)

    if camera_name == "ZED":
        svo_file = f"{get_root_path()}/resources/{baby}/{age}/rgbd/{camera_name}.svo"
        vitpose.run_with_zed(svo_file, idx_from=8010, idx_to=8910)
    else:
        vitpose.run_with_rs(stream_root, rotation_angle=0)
