import time

import cv2
import numpy as np
import pyrealsense2 as rs
from mediapipe.tasks.python.components.containers.landmark import Landmark, NormalizedLandmark

from src.detection.mediapipe_pose_detector import MPModel, MPPoseDetector
from src.enums.mp_enum import MPPoseLandmark
from src.utils.csv_writer import CSVWriter
from src.utils.path import get_root_path
from src.utils.plot_utils import rotate_dim, rotate_img
from src.utils.video_writer import VideoWriter, ColorDepthReader


class MediapipeOffline:
    def __init__(self, input_file: str = None, model: MPModel = MPModel.FULL):
        self._pose_detector = MPPoseDetector(model)
        self._stream_reader = ColorDepthReader(input_file)

    def run_with_rs(self, output_video_file: str, output_csv_file: str, coco_on: bool = True):
        # Neither output file cannot be None
        assert output_video_file is not None and output_csv_file is not None, "Output files cannot be None."
        csv_writer = CSVWriter(output_csv_file)

        roi = None

        rotation_angle: int = 0
        frame_dim = rotate_dim(self._stream_reader.get_frame_size(), rotation_angle)
        idx: int = 0
        is_paused = False  # Variable to check if the video is paused or not

        video_writer = VideoWriter(output_video_file, self._stream_reader.get_fps(), frame_dim)
        while True:
            key_pressed = cv2.waitKey(1) & 0xFF
            # if Space is pressed, toggle the pause state
            if key_pressed == ord(' '):
                is_paused = not is_paused
            # if Q is pressed, exit the loop
            elif key_pressed == ord('q'):
                break

            # Only grab the next frame if the video is not paused
            # if N is pressed and the video is paused, grab the next frame
            if is_paused and not key_pressed == ord('n'):
                continue

            idx += 1
            ret_color, color_image, ret_depth, depth_image = self._stream_reader.get_frames()

            if not ret_color or not ret_depth:
                print("No more frames to read.")
                break

            color_image = rotate_img(color_image, rotation_angle)
            depth_image = rotate_img(depth_image, rotation_angle)
            annotated_image = color_image.copy()

            if roi is None:
                # Crop both images with ROI (Region of Interest)
                roi = cv2.selectROI("Color Image", color_image)

            x, y, w, h = roi
            roi_color_image = color_image[y:y + h, x:x + w, :].astype(np.uint8)

            timestamp = int(self._stream_reader.get_timestamp())
            pose_landmarks = self._pose_detector.detect_pose(roi_color_image, timestamp)
            roi_annotated_image = MPPoseDetector.draw_landmarks_on_image(roi_color_image, pose_landmarks)
            cv2.imshow("ROI Annotated Image", roi_annotated_image)

            written: bool = False
            for pose_world_landmarks, normalized_landmarks in zip(pose_landmarks.pose_world_landmarks,
                                                                  pose_landmarks.pose_landmarks):
                for landmark_id, (landmark_xyz, landmark_xy) in enumerate(
                        zip(pose_world_landmarks, normalized_landmarks)):

                    mp_pose_lm = MPPoseLandmark(landmark_id)
                    if (coco_on and mp_pose_lm.is_coco()) or not coco_on:
                        # Translate the normalized landmark to the image coordinate system
                        if roi is not None:
                            landmark_xy.x = int(landmark_xy.x * roi_color_image.shape[1]) + x
                            landmark_xy.y = int(landmark_xy.y * roi_color_image.shape[0]) + y
                            # show the pixels on the image
                            cv2.circle(annotated_image, (landmark_xy.x, landmark_xy.y), 4, (0, 255, 0), -1)
                        else:
                            landmark_xy.x = int(landmark_xy.x * color_image.shape[1])
                            landmark_xy.y = int(landmark_xy.y * color_image.shape[0])

                        pixel_coordinates = np.array([landmark_xy.x, landmark_xy.y])
                        depth = depth_image[pixel_coordinates[1], pixel_coordinates[0]] / 1000
                        world_coordinates = rs.rs2_deproject_pixel_to_point(self._stream_reader.intrinsics,
                                                                            pixel_coordinates, depth)

                        # TODO: Both XY and XYZ coordinates will be LATER transposed to calibrated ORIGIN

                        csv_writer.write(idx, idx, 0, landmark_xyz, landmark_xy, world_coordinates,
                                         mp_pose_lm)
                        written = True

            if not written:  # If no landmarks detected, write an empty landmark
                empty_landmark_xyz = Landmark(0, 0, 0, 0, 0)
                empty_landmark_xy = NormalizedLandmark(0, 0, 0)
                world_coordinates = (0, 0, 0)
                for empty_lm in MPPoseLandmark:
                    if (coco_on and empty_lm.is_coco()) or not coco_on:
                        csv_writer.write(idx, idx, 0, empty_landmark_xyz, empty_landmark_xy,
                                         world_coordinates, empty_lm)
                # print("No landmarks detected.")

            # Write the annotated image to the video file
            video_writer.write_frame(annotated_image)

            cv2.imshow("Annotated Image", annotated_image)

        video_writer.close()
        cv2.destroyAllWindows()
        self._stream_reader.close()


if __name__ == "__main__":
    start = time.time()

    camera_name = "D435"
    # camera_name = "D455"
    # camera_name = "ZED"

    baby = "NT"
    age = "27w"
    parent_folder = f"{get_root_path()}/output/{baby}/{age}/{camera_name}/"
    filename = f"{camera_name}_short"
    mp_model = MPModel.HEAVY
    realsense_mp = MediapipeOffline(parent_folder + filename, mp_model)

    video_file = f"{get_root_path()}/output/{baby}/{age}/{camera_name}/{mp_model}/{filename}_x_{mp_model}.mp4"
    csv_file = f"{get_root_path()}/output/{baby}/{age}/{camera_name}/{mp_model}/{filename}_x_{mp_model}.csv"
    realsense_mp.run_with_rs(video_file, csv_file)

    print(f"Elapsed time: {time.time() - start:.2f} seconds.")
