import cv2
import mediapipe as mp
from mediapipe import solutions
from mediapipe.framework.formats import landmark_pb2
from mediapipe.tasks.python.vision import PoseLandmarker
from mediapipe.tasks.python.vision import PoseLandmarkerResult
import numpy as np

from src.enums.coco_enum import CoCo
from src.enums.mp_enum import MPPoseLandmark
from src.utils.csv_writer import CSVWriter
from src.utils.path import get_root_path
from src.utils.video_writer import VideoWriter


# Enum of MP models. Lite, Full, and Heavy models are available.
class MPModel:
    LITE = "lite"
    FULL = "full"
    HEAVY = "heavy"
    NATIVE = "native"  # Not available for MediaPipe Pose Landmark. Use for ZED Pose Landmark
    VITPOSE = "vitpose"  # Not available for MediaPipe Pose Landmark. Use for ViTPose


class MPPoseDetector:
    def __init__(self, model: MPModel = MPModel.LITE):
        self._used_model = model
        self._model_path = self.get_mp_model_path(model)
        self._pose_landmarker = self._create_landmarker()

    def _create_landmarker(self):
        base_options = mp.tasks.BaseOptions
        pose_landmarker = mp.tasks.vision.PoseLandmarker
        pose_landmarker_options = mp.tasks.vision.PoseLandmarkerOptions
        vision_running_mode = mp.tasks.vision.RunningMode

        # Create a pose landmarker instance with the video mode:
        options = pose_landmarker_options(
            base_options=base_options(model_asset_path=self._model_path),
            num_poses=1,  # Default
            running_mode=vision_running_mode.VIDEO)

        return pose_landmarker.create_from_options(options)

    def detect_pose(self, rgb_image: np.ndarray, timestamp: int) -> PoseLandmarkerResult:
        # Convert the frame received from OpenCV to a MediaPipe’s Image object.
        mp_image = mp.Image(image_format=mp.ImageFormat.SRGB, data=rgb_image)

        # Perform pose landmark detection on the provided single image.
        # The pose landmarker must be created with the video mode.
        pose_landmarker_result = self._pose_landmarker.detect_for_video(mp_image, timestamp)

        return pose_landmarker_result

    def from_video(self, video_path: str, output_path: str, csv_path: str = None, coco_on: bool = True):
        cap = cv2.VideoCapture(video_path)
        video_writer = VideoWriter(output_path, cap.get(cv2.CAP_PROP_FPS),
                                   (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))
        csv_writer = CSVWriter(csv_path) if csv_path is not None else None

        idx: int = 0
        while cap.isOpened():
            success, color_image = cap.read()
            idx += 1
            if not success:
                break

            timestamp = int(cap.get(cv2.CAP_PROP_POS_MSEC))
            pose_landmarks = self.detect_pose(color_image, timestamp)
            annotated_image = self.draw_landmarks_on_image(color_image, pose_landmarks)

            if csv_writer is not None:
                for pose_world_landmarks, normalized_landmarks in zip(pose_landmarks.pose_world_landmarks,
                                                                      pose_landmarks.pose_landmarks):
                    for landmark_id, (landmark_xyz, landmark_xy) in enumerate(
                            zip(pose_world_landmarks, normalized_landmarks)):

                        mp_pose_lm = MPPoseLandmark(landmark_id)
                        if (coco_on and mp_pose_lm.name in CoCo.__members__.keys()) or not coco_on:
                            # Translate the normalized landmark to the image coordinate system
                            landmark_xy.x = int(landmark_xy.x * color_image.shape[1])
                            landmark_xy.y = int(landmark_xy.y * color_image.shape[0])
                            csv_writer.write(idx, idx, timestamp, landmark_xyz, landmark_xy, (0, 0, 0), mp_pose_lm)

            # Write the annotated image to the video file
            video_writer.write_frame(annotated_image)

            cv2.imshow("Annotated Image", annotated_image)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cap.release()
        video_writer.close()
        cv2.destroyAllWindows()

    def reproject_to_file(self, video_path: str, reproject_path: str, output_path: str):
        """
        Params:
        video_path: Path to the video file.
        reproject_path: Path to the reprojected video file.
        output_path: Path to the output video file. It will contain the annotated and reprojected video.
        """
        cap = cv2.VideoCapture(video_path)
        reproject = cv2.VideoCapture(reproject_path)
        video_writer = VideoWriter(output_path, cap.get(cv2.CAP_PROP_FPS),
                                   (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))

        while cap.isOpened():
            success, color_image = cap.read()
            if not success:
                break
            success, reproject_image = reproject.read()
            if not success:
                break

            timestamp = int(cap.get(cv2.CAP_PROP_POS_MSEC))
            pose_landmarks = self.detect_pose(color_image, timestamp)
            annotated_image = self.draw_landmarks_on_image(reproject_image, pose_landmarks)

            # Write the annotated image to the video file
            video_writer.write_frame(annotated_image)

            cv2.imshow("Annotated Image", annotated_image)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cap.release()
        reproject.release()
        video_writer.close()
        cv2.destroyAllWindows()

    @staticmethod
    def get_mp_model_path(mp_model: MPModel):
        if mp_model == MPModel.LITE:
            return get_root_path() + "/models/pose_landmarker_lite.task"
        elif mp_model == MPModel.FULL:
            return get_root_path() + "/models/pose_landmarker_full.task"
        elif mp_model == MPModel.HEAVY:
            return get_root_path() + "/models/pose_landmarker_heavy.task"
        else:
            raise ValueError("Invalid model type. Choose from 'lite', 'full', or 'heavy'.")

    @staticmethod
    def draw_landmarks_on_image(rgb_image: np.ndarray, detection_result: PoseLandmarkerResult) -> np.ndarray:
        pose_landmarks_list = detection_result.pose_landmarks
        annotated_image = np.copy(rgb_image)

        # Loop through the detected poses to visualize.
        for idx in range(len(pose_landmarks_list)):
            pose_landmarks = pose_landmarks_list[idx]

            # Draw the pose landmarks.
            pose_landmarks_proto = landmark_pb2.NormalizedLandmarkList()
            pose_landmarks_proto.landmark.extend([
                landmark_pb2.NormalizedLandmark(x=landmark.x, y=landmark.y, z=landmark.z) for landmark in pose_landmarks
            ])
            solutions.drawing_utils.draw_landmarks(
                annotated_image,
                pose_landmarks_proto,
                solutions.pose.POSE_CONNECTIONS,
                solutions.drawing_styles.get_default_pose_landmarks_style())
        return annotated_image

    def get_used_model(self) -> MPModel:
        return self._used_model

    def close(self):
        self._pose_landmarker.close()


if __name__ == "__main__":
    model_level = MPModel.FULL
    coco_on = True  # Set to False in order to disable CoCo landmarks
    pose_detector = MPPoseDetector(model_level)

    video_path = get_root_path() + "/output/FI/32w/D435/D435_short_color.mp4"
    output_path = get_root_path() + "/output/FI/32w/D435/D435_short_annotated.avi"
    csv_path = get_root_path() + "/output/FI/32w/D435/D435_short_annotated.csv"

    """ Running pose detector on Video file and saving the annotated video and CSV file."""
    pose_detector.from_video(video_path, output_path, csv_path, coco_on)

    """ Reprojecting the detected pose landmarks to the original video."""
    # reproject_path = get_root_path() + "/resources/" + infant_path + "_ViTPose.avi"
    # pose_detector.reproject_to_file(video_path, reproject_path, output_path)

    pose_detector.close()
