import cv2
import numpy as np

from src.streaming.cameras.realsense import RealsenseCamera
from src.streaming.cameras.zed import ZedCamera
from src.utils.csv_writer import CSVWriter
from src.utils.path import get_root_path
from src.utils.plot_utils import rotate_img

# Global flag
mouse_clicked = False


def mouse_callback(event, x, y, flags, param):
    global mouse_clicked
    if event == cv2.EVENT_LBUTTONDOWN:
        param[0] = np.array([x, y])
        print(f"Origin: {param[0]}")
        mouse_clicked = True  # set the flag to True


def calibrate(camera: ZedCamera | RealsenseCamera, output_csv_file: str, rot_angle: int = 0):
    assert output_csv_file is not None, "Output file cannot be None."
    csv_writer = CSVWriter(output_csv_file)

    is_paused = False  # Variable to check if the video is paused or not
    origin = None
    last_timestamp = 0

    idx = 0
    while True:
        key_pressed = cv2.waitKey(1) & 0xFF
        # if Space is pressed, toggle the pause state
        if key_pressed == ord(' '):
            is_paused = not is_paused
        # if Q is pressed, exit the loop
        elif key_pressed == ord('q'):
            break

        # Only grab the next frame if the video is not paused
        # if N is pressed and the video is paused, grab the next frame
        if is_paused and not key_pressed == ord('n'):
            continue

        idx += 1
        camera.run()

        # Get the output image
        color_image = camera.get_rgb_img().copy()
        color_image = rotate_img(color_image, rot_angle)

        timestamp = int(camera.get_timestamp())
        if last_timestamp > timestamp:
            raise ValueError(f"Timestamps are not in order: {last_timestamp} > {timestamp}. Probably EOF.")
        last_timestamp = timestamp

        # Choose a pixel in the image to be the origin
        if origin is None:
            cv2.imshow("ROI Color Image", color_image)
            print("Click on the origin pixel.")
            origin = [None]  # mutable, so we can modify it inside the callback
            cv2.setMouseCallback("ROI Color Image", mouse_callback, origin)
            while not mouse_clicked:  # wait until the mouse is clicked
                cv2.waitKey(1)
            cv2.destroyWindow("ROI Color Image")  # destroy the window after the click
            origin = origin[0]  # get the value from the list

        # Draw the origin pixel
        cv2.circle(color_image, origin, 5, (0, 0, 255), -1)

        # Deproject the origin pixel
        depth_image = camera.get_depth_img().copy()
        depth_image = rotate_img(depth_image, rot_angle)
        origin_xyz = camera.deproject_pixel(origin, depth_image)
        print(f"Origin XYZ: {origin_xyz}")

        frame_number = camera.get_frame_number()
        # Write to CSV
        csv_writer.write(idx, frame_number, timestamp, (0, 0, 0), origin, origin_xyz, "Origin")

        cv2.imshow("Camera", color_image)

    camera.close()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    camera_name = "D435"
    # camera_name = "D455"
    # camera_name = "ZED"

    baby = "NT"
    age = "27w"
    parent_folder = f"resources/{baby}/{age}/calibration"
    filename = f"{camera_name}"

    ends = ".bag" if camera_name == "D455" or camera_name == "D435" else ".svo"
    input_file = f"{get_root_path()}/{parent_folder}/{filename}{ends}"

    if camera_name == "ZED":
        camera = ZedCamera(input_file, save=False, body_tracking=False, flip=True)
    else:
        camera = RealsenseCamera(input_file, save=False)

    calibration_output_file = f"{get_root_path()}/output/{baby}/{age}/{camera_name}/{filename}_origin.csv"
    calibrate(camera, calibration_output_file, rot_angle=0)
