import cv2

from src.enums.defaults import DEFAULT_FPS
from src.streaming.cameras.realsense import RealsenseCamera
from src.streaming.cameras.zed import ZedCamera
from src.utils.path import get_root_path
from src.utils.plot_utils import rotate_dim, rotate_img
from src.utils.video_writer import ColorDepthWriter


def cut_color_depth_stream(camera: RealsenseCamera | ZedCamera, output_file: str, rotation_angle: int = 0,
                           idx_from: int = 0, idx_to: int = 0):
    idx: int = 0
    is_paused = False  # Variable to check if the video is paused or not
    last_timestamp = 0
    last_frame_number = 0
    recording = False

    frame_dim = rotate_dim(camera.get_defaults_dimensions(), rotation_angle)
    video_writer = ColorDepthWriter(output_file, DEFAULT_FPS, frame_dim, camera.get_intrinsics())
    while True:
        # if Realsense camera, use frame_number as get_frame_number()
        frame_number = camera.get_frame_number()
        if idx_to != 0 and (
                frame_number == idx_from or frame_number == idx_to):  # My custom condition to stop the video
            recording = not recording
            if recording:
                video_writer = ColorDepthWriter(output_file, DEFAULT_FPS, frame_dim, camera.get_intrinsics())
            else:
                video_writer.close()
                break

        key_pressed = cv2.waitKey(1) & 0xFF
        # if Space is pressed, toggle the pause state
        if key_pressed == ord(' '):
            is_paused = not is_paused
        # if Q is pressed, exit the loop
        elif key_pressed == ord('q'):
            break

        # Only grab the next frame if the video is not paused
        # if N is pressed and the video is paused, grab the next frame
        if is_paused and not key_pressed == ord('n'):
            continue

        idx += 1
        camera.run()

        # Get the output image
        color_image = camera.get_rgb_img().copy()
        color_image = rotate_img(color_image, rotation_angle)

        depth_image = camera.get_depth_img().copy()
        depth_image = rotate_img(depth_image, rotation_angle)

        timestamp = int(camera.get_timestamp())
        if last_timestamp > timestamp:
            raise ValueError(f"Timestamps are not in order: {last_timestamp} > {timestamp}. Probably EOF.")
        last_timestamp = timestamp

        frame_number = camera.get_frame_number()
        if frame_number != last_frame_number + 1 and idx != 1:
            print(f"Frame number: {frame_number}, last frame number: {last_frame_number}")
            if recording:
                diff = frame_number - last_frame_number - 1
                print(f"Diff: {diff}")
                for i in range(diff - 1):
                    video_writer.write_frames(color_image, depth_image)

        last_frame_number = frame_number

        if recording:
            video_writer.write_frames(color_image, depth_image)

        # Put some info on the image
        camera.put_info_to_image(color_image, idx)

        cv2.imshow("Camera", color_image)

    camera.close()
    video_writer.close()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    # camera_name = "D435"
    # camera_name = "D455"
    camera_name = "ZED"

    baby = "NT"
    age = "27w"
    parent_folder = f"resources/{baby}/{age}/rgbd"
    filename = f"{camera_name}"

    ends = ".bag" if camera_name == "D455" or camera_name == "D435" else ".svo"
    input_file = f"{get_root_path()}/{parent_folder}/{filename}{ends}"

    if camera_name == "ZED":
        camera = ZedCamera(input_file, save=False, body_tracking=False, flip=True)
    else:
        camera = RealsenseCamera(input_file, save=False)

    output_file = f"{get_root_path()}/output/{baby}/{age}/{camera_name}/{filename}_short"
    cut_color_depth_stream(camera, output_file, rotation_angle=90, idx_from=8010, idx_to=8910)
