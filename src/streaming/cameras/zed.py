import os
import time

import numpy as np
import pyzed.sl as sl
import cv2
import math

from src.streaming.cameras.realsense import DEFAULT_FPS
from src.enums.defaults import ZED_DEFAULT_HEIGHT, ZED_DEFAULT_WIDTH
from src.utils.plot_utils import time_to_str


class ZedCamera:
    def __init__(self, svo_file: str = None, save: bool = False, body_tracking: bool = False, flip: bool = False):
        self._camera = sl.Camera()
        self._is_flipped = flip
        self._init_camera_params()
        self._init_and_open_camera(svo_file, save)

        self.body_tracking_on = body_tracking
        self._init_body_params() if body_tracking else None

        self._print_info()  # Print some information about the camera
        self._runtime_param = sl.RuntimeParameters()

        self._img_rgb = sl.Mat()
        self._ply = sl.Mat()
        self._resolution = self._camera.get_camera_information().camera_configuration.resolution
        self._point_cloud = sl.Mat(self._resolution.width, self._resolution.height, sl.MAT_TYPE.F32_C4, sl.MEM.CPU)

        self._start_time = None  # Start time of the camera

    def _init_camera_params(self):
        self._init_params = sl.InitParameters()
        self._init_params.depth_mode = sl.DEPTH_MODE.ULTRA
        self._init_params.coordinate_units = sl.UNIT.METER
        self._init_params.coordinate_system = sl.COORDINATE_SYSTEM.RIGHT_HANDED_Y_UP
        self._init_params.camera_resolution = sl.RESOLUTION.HD720  # Dependent on DEFAULT WIDTH and HEIGHT
        self._init_params.camera_fps = DEFAULT_FPS
        self._init_params.camera_image_flip = sl.FLIP_MODE.ON if self._is_flipped else sl.FLIP_MODE.OFF
        self._init_params.sdk_verbose = 1

    def _init_and_open_camera(self, svo_file: str | None, save: bool):
        if svo_file is not None and not save:
            assert os.path.isfile(svo_file), f"SVO file {svo_file} does not exist."
            assert svo_file.endswith(".svo"), f"Invalid SVO file {svo_file}."
            input_type = sl.InputType()
            input_type.set_from_svo_file(svo_file)
            self._init_params.svo_real_time_mode = False
            self._init_params.input = input_type

        status = self._camera.open(self._init_params)
        assert status == sl.ERROR_CODE.SUCCESS, f"Could not open ZED camera ({status})."

        if svo_file:
            # print out the length of the video
            svo_info = self._camera.get_svo_number_of_frames()
            print(f"Number of frames in the SVO file: {svo_info}")

        if svo_file is not None and save:
            recording_param = sl.RecordingParameters(video_filename=svo_file,
                                                     compression_mode=sl.SVO_COMPRESSION_MODE.LOSSLESS,
                                                     target_framerate=DEFAULT_FPS)
            err = self._camera.enable_recording(recording_param)
            assert err == sl.ERROR_CODE.SUCCESS, f"Could not enable recording ({err})."

    def _init_body_params(self):
        # Initialize body tracking parameters
        self._body_params = sl.BodyTrackingParameters()
        self._body_params.detection_model = sl.BODY_TRACKING_MODEL.HUMAN_BODY_ACCURATE
        self._body_params.enable_tracking = True
        self._body_params.image_sync = True
        self._body_params.enable_segmentation = False
        self._body_params.enable_body_fitting = True
        self._body_params.body_format = sl.BODY_FORMAT.BODY_18

        if self._body_params.enable_tracking:
            positional_tracking_param = sl.PositionalTrackingParameters()
            positional_tracking_param.set_floor_as_origin = True
            self._camera.enable_positional_tracking(positional_tracking_param)

        err = self._camera.enable_body_tracking(self._body_params)
        assert err == sl.ERROR_CODE.SUCCESS, f"Could not enable body tracking ({err})."

        self._bodies = sl.Bodies()

        self._body_runtime_param = sl.BodyTrackingRuntimeParameters()
        self._body_runtime_param.detection_confidence_threshold = 40

    def _print_info(self):
        cam_info = self._camera.get_camera_information()
        print("ZED Model                 : {0}".format(cam_info.camera_model))
        print("ZED Serial Number         : {0}".format(cam_info.serial_number))
        print("ZED Camera Firmware       : {0}/{1}".format(cam_info.camera_configuration.firmware_version,
                                                           cam_info.sensors_configuration.firmware_version))
        print("ZED Camera Resolution     : {0}x{1}".format(round(cam_info.camera_configuration.resolution.width, 2),
                                                           cam_info.camera_configuration.resolution.height))
        print("ZED Camera FPS            : {0}".format(int(cam_info.camera_configuration.fps)))

    def run(self) -> None:
        err = self._camera.grab(self._runtime_param)
        if err != sl.ERROR_CODE.SUCCESS:
            print("Could not grab image from ZED camera. Probably SVO-EOF reached.")
            exit()

        self._camera.retrieve_image(self._img_rgb, sl.VIEW.LEFT)
        self._camera.retrieve_measure(self._ply, sl.MEASURE.XYZRGBA)  # Matrix for point cloud measurements

        # Retrieve detected bodies
        self._camera.retrieve_bodies(self._bodies, self._body_runtime_param) if self.body_tracking_on else None

        # Start the timer if it is the first frame
        self._start_time = time.time() if self._start_time is None else self._start_time

    def deproject_pixels(self, pixels: np.ndarray, depth_image: np.ndarray) -> np.ndarray:
        """
        :param pixels: 2D array with two elements in each row is expected.
        :param depth_image: Depth image.
        :returns: Array of [x, y, z] coordinates with center at the camera.
        """
        return np.apply_along_axis(self.deproject_pixel, axis=1, arr=pixels, depth_image=depth_image)

    def deproject_pixel(self, pixel: np.ndarray, depth_image: np.ndarray | None = None) -> np.ndarray:
        """
        :param pixel: 1D array with two elements is expected.
        :param depth_image: Depth image.
        :returns: Array of [x, y, z] coordinates with center at the camera.
        """
        if pixel[0] < 0 or pixel[0] >= self._resolution.width or pixel[1] < 0 or pixel[1] >= self._resolution.height:
            return np.array([np.nan, np.nan, np.nan])

        err = sl.ERROR_CODE.SUCCESS
        if depth_image is None:
            err, point_cloud_value = self._ply.get_value(int(pixel[0]), int(pixel[1]))
        else:
            point_cloud_value = depth_image[int(pixel[1]), int(pixel[0])]
        if err != sl.ERROR_CODE.SUCCESS or not math.isfinite(point_cloud_value[2]):
            # print("Could not get value from point cloud.")
            return np.array([0, 0, 0])
        return np.array([point_cloud_value[0], point_cloud_value[1], point_cloud_value[2]])

    def deproject_image(self, roi=None, depth_image: np.ndarray | None = None) -> np.ndarray:
        # Deproject all pixels in the image
        if depth_image is None:
            depth_image = self.get_depth_img().copy()

        height, width = depth_image.shape[:2]
        if roi is not None:
            x, y, w, h = roi
            pixels = np.array([[i, j] for i in range(x, x + w) for j in range(y, y + h)])
        else:
            pixels = np.array([[x, y] for x in range(width) for y in range(height)])
        return self.deproject_pixels(pixels, depth_image)

    def get_distance(self, x: int, y: int) -> float:
        # Get and print distance value in meters
        # We measure the distance camera - object using Euclidean distance
        err, point_cloud_value = self._ply.get_value(x, y)
        if err == sl.ERROR_CODE.SUCCESS and math.isfinite(point_cloud_value[2]):
            distance = np.linalg.norm(point_cloud_value[:3])
            return distance
        else:
            return 0

    @staticmethod
    def draw_keypoints(output_image: np.ndarray, bodies):
        people_detected = len(bodies)
        # Put the number of detected people on the image
        cv2.putText(output_image, "Detected : " + str(people_detected), (20, 70), cv2.FONT_HERSHEY_SIMPLEX, 1,
                    (0, 255, 0), 2)
        for body in bodies:
            for keypoint in body.keypoint_2d:
                cv2.circle(output_image, (int(keypoint[0]), int(keypoint[1])), 5, (0, 255, 0), -1)

    def put_info_to_image(self, output_image: np.ndarray, idx: int):
        """
        :param output_image: Image to put the information.
        :param idx: Index of the color frame from which the image is taken.
        :returns: Image with information.
        """
        font_scale = 0.5
        color = (255, 0, 0)  # BGR
        thickness = 2

        # Calculate elapsed time
        elapsed_time = self.get_elapsed_time()
        elapsed_time_str = time_to_str(elapsed_time)
        # Put the elapsed time on the image
        cv2.putText(output_image, "Time: " + elapsed_time_str, (20, 50), cv2.FONT_HERSHEY_SIMPLEX, font_scale,
                    color, thickness)
        # Put the frame number on the image
        frame_number = self.get_frame_number()
        cv2.putText(output_image, "Frame Number: " + str(frame_number), (20, 100), cv2.FONT_HERSHEY_SIMPLEX, font_scale,
                    color, thickness)
        # Put the index on the image
        cv2.putText(output_image, "Frame Index: " + str(idx), (20, 150), cv2.FONT_HERSHEY_SIMPLEX, font_scale,
                    color, thickness)
        # Put timestamp on the image
        timestamp = self.get_timestamp()
        cv2.putText(output_image, "Timestamp: " + str(timestamp), (20, 200), cv2.FONT_HERSHEY_SIMPLEX, font_scale,
                    color, thickness)

    def set_svo_position(self, position: int) -> None:
        self._camera.set_svo_position(position)

    def get_camera_name(self) -> str:
        return "ZED"

    def get_intrinsics(self) -> sl.CameraParameters:
        return self._camera.get_camera_information().camera_configuration.calibration_parameters.left_cam

    def get_fps(self) -> int:
        return int(self._camera.get_camera_information().camera_configuration.fps)

    def get_bodies(self):
        return self._bodies.body_list

    def get_rgb_img(self) -> np.ndarray:
        return cv2.cvtColor(self._img_rgb.get_data(), cv2.COLOR_BGRA2BGR)  # We do not need alpha channel

    def get_depth_img(self) -> np.ndarray:
        return self._ply.get_data()  # XYZ + 4th channel

    def get_timestamp(self):
        return self._camera.get_timestamp(sl.TIME_REFERENCE.IMAGE).get_milliseconds()

    def get_frame_number(self):
        return self._camera.get_svo_position()
        # return self._camera.get_timestamp(sl.TIME_REFERENCE.CURRENT).get_milliseconds()

    def get_elapsed_time(self) -> float:
        return time.time() - self._start_time  # Elapsed time since the camera started

    @staticmethod
    def get_defaults_dimensions() -> tuple[int, int]:
        return ZED_DEFAULT_WIDTH, ZED_DEFAULT_HEIGHT

    def close(self):
        self._camera.close()


"""
Main to run ZED camera.
OBSOLETE: This is not used anymore. It is kept for reference.
"""
# if __name__ == "__main__":
#     parent_folder = "resources/baby_vrlab_3_4_2024/real"
#     filename = "baby_real_fullhd_ZED"
#
#     svo_file = get_root_path() + "/" + parent_folder + "/" + filename + ".svo"
#     save = False
#     camera = ZedCamera(svo_file, save, body_tracking=False)
#     # camera = ZedCamera()
#
#     rotation_angle: int = 0
#     is_paused = False  # Variable to check if the video is paused or not
#
#     while True:
#         key_pressed = cv2.waitKey(1) & 0xFF
#         # if Space is pressed, toggle the pause state
#         if key_pressed == ord(' '):
#             is_paused = not is_paused
#         # if N is pressed and the video is paused, grab the next frame
#         elif key_pressed == ord('n') and is_paused:
#             camera.run()
#         # if Q is pressed, exit the loop
#         elif key_pressed == ord('q'):
#             break
#         # if R is pressed, rotate the image
#         elif key_pressed == ord('r'):
#             rotation_angle += 90
#             rotation_angle %= 360
#
#         # Get the output image
#         output_image = camera.get_rgb_img().copy()
#         output_image = rotate(output_image, rotation_angle)
#
#         # Only grab the next frame if the video is not paused
#         if not is_paused:
#             camera.run()
#
#         # Calculate elapsed time
#         elapsed_time = camera.get_elapsed_time()
#         elapsed_time_str = time_to_str(elapsed_time)
#         # Put the elapsed time on the image
#         cv2.putText(output_image, "Time: " + elapsed_time_str, (20, 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
#         cv2.imshow("ZED Camera", output_image)
#
#     camera.close()
#     cv2.destroyAllWindows()
