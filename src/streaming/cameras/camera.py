import cv2
import numpy as np

from src.streaming.cameras.realsense import RealsenseCamera
from src.streaming.cameras.zed import ZedCamera
from src.enums.defaults import DEFAULT_FPS
from src.utils.csv_writer import CSVWriter
from src.utils.path import get_root_path
from src.utils.plot_utils import rotate_img, rotate_dim
from src.utils.video_writer import VideoWriter, ColorDepthWriter

# Global flag
mouse_clicked = False


def mouse_callback(event, x, y, flags, param):
    global mouse_clicked
    if event == cv2.EVENT_LBUTTONDOWN:
        param[0] = np.array([x, y])
        print(f"Origin: {param[0]}")
        mouse_clicked = True  # set the flag to True


class Camera:
    def __init__(self, input_file: str, save: bool):
        # If input file is .svo file, use ZedCamera
        if input_file.endswith(".svo"):
            self.camera = ZedCamera(input_file, save, body_tracking=False, flip=True)
        # If input file is .bag file, use RealsenseCamera
        elif input_file.endswith(".bag"):
            self.camera = RealsenseCamera(input_file, save)
        else:
            raise ValueError(f"Invalid input file {input_file}.")

    def calibrate(self, output_csv_file: str):
        assert output_csv_file is not None, "Output file cannot be None."
        csv_writer = CSVWriter(output_csv_file)

        is_paused = False  # Variable to check if the video is paused or not
        origin = None
        last_timestamp = 0

        idx = 0
        while True:
            key_pressed = cv2.waitKey(1) & 0xFF
            # if Space is pressed, toggle the pause state
            if key_pressed == ord(' '):
                is_paused = not is_paused
            # if Q is pressed, exit the loop
            elif key_pressed == ord('q'):
                break

            # Only grab the next frame if the video is not paused
            # if N is pressed and the video is paused, grab the next frame
            if is_paused and not key_pressed == ord('n'):
                continue

            idx += 1
            self.camera.run()

            # Get the output image
            color_image = self.camera.get_rgb_img().copy()
            rot_angle = 0
            color_image = rotate_img(color_image, rot_angle)

            timestamp = int(self.camera.get_timestamp())
            if last_timestamp > timestamp:
                raise ValueError(f"Timestamps are not in order: {last_timestamp} > {timestamp}. Probably EOF.")
            last_timestamp = timestamp

            # Choose a pixel in the image to be the origin
            if origin is None:
                cv2.imshow("ROI Color Image", color_image)
                print("Click on the origin pixel.")
                origin = [None]  # mutable, so we can modify it inside the callback
                cv2.setMouseCallback("ROI Color Image", mouse_callback, origin)
                while not mouse_clicked:  # wait until the mouse is clicked
                    cv2.waitKey(1)
                cv2.destroyWindow("ROI Color Image")  # destroy the window after the click
                origin = origin[0]  # get the value from the list

            # Draw the origin pixel
            cv2.circle(color_image, origin, 5, (0, 0, 255), -1)

            # Deproject the origin pixel
            depth_image = self.camera.get_depth_img().copy()
            depth_image = rotate_img(depth_image, rot_angle)
            origin_xyz = self.camera.deproject_pixel(origin, depth_image)
            print(f"Origin XYZ: {origin_xyz}")

            frame_number = self.camera.get_frame_number()
            # Write to CSV
            csv_writer.write(idx, frame_number, timestamp, (0, 0, 0), origin, origin_xyz, "Origin")

            cv2.imshow("Camera", color_image)

        self.camera.close()
        cv2.destroyAllWindows()

    def cut_rgb_stream(self, output_video_file: str, idx_from: int = 0, idx_to: int = 0):
        assert output_video_file is not None, "Output files cannot be None."
        frame_dim = self.camera.get_defaults_dimensions()
        video_writer = VideoWriter(output_video_file, DEFAULT_FPS, frame_dim)

        rotation_angle: int = 0
        idx: int = 0
        is_paused = False  # Variable to check if the video is paused or not
        last_timestamp = 0
        recording = False

        while True:
            # if Realsense camera, use frame_number as get_frame_number()
            frame_number = self.camera.get_frame_number() if isinstance(self.camera, RealsenseCamera) else idx
            if idx_to != 0 and (
                    frame_number == idx_from or frame_number == idx_to):  # My custom condition to stop the video
                recording = not recording
                if recording:
                    video_writer = VideoWriter(output_video_file, DEFAULT_FPS, frame_dim)
                else:
                    video_writer.close()
                    break

            key_pressed = cv2.waitKey(1) & 0xFF
            # if Space is pressed, toggle the pause state
            if key_pressed == ord(' '):
                is_paused = not is_paused
            # if Q is pressed, exit the loop
            elif key_pressed == ord('q'):
                break
            # if R is pressed, rotate the image
            elif key_pressed == ord('r'):
                # TODO: Not working properly. Problem with ZED camera.
                rotation_angle += 90
                rotation_angle %= 360
                # Reinitialize the video writer with the rotated dimensions
                frame_dim = rotate_dim(frame_dim, rotation_angle)
                video_writer = VideoWriter(output_video_file, DEFAULT_FPS, frame_dim)
            # if S is pressed, start recording
            elif key_pressed == ord("s"):
                recording = not recording

            # Only grab the next frame if the video is not paused
            # if N is pressed and the video is paused, grab the next frame
            if is_paused and not key_pressed == ord('n'):
                continue

            idx += 1
            self.camera.run()

            # Get the output image
            output_image = self.camera.get_rgb_img().copy()
            output_image = rotate_img(output_image, rotation_angle)

            timestamp = int(self.camera.get_timestamp())
            if last_timestamp > timestamp:
                raise ValueError(f"Timestamps are not in order: {last_timestamp} > {timestamp}. Probably EOF.")
            last_timestamp = timestamp

            if recording:
                video_writer.write_frame(output_image)

            # Put some info on the image
            self.camera.put_info_to_image(output_image, idx)

            cv2.imshow("Camera", output_image)

        self.camera.close()
        video_writer.close()
        cv2.destroyAllWindows()

    def cut_color_depth_stream(self, output_file: str, idx_from: int = 0, idx_to: int = 0):
        rotation_angle: int = 90
        idx: int = 0
        is_paused = False  # Variable to check if the video is paused or not
        last_timestamp = 0
        last_frame_number = 0
        recording = False

        frame_dim = rotate_dim(self.camera.get_defaults_dimensions(), rotation_angle)
        video_writer = ColorDepthWriter(output_file, DEFAULT_FPS, frame_dim, self.camera.get_intrinsics())
        while True:
            # if Realsense camera, use frame_number as get_frame_number()
            frame_number = self.camera.get_frame_number() if isinstance(self.camera, RealsenseCamera) else idx
            if idx_to != 0 and (
                    frame_number == idx_from or frame_number == idx_to):  # My custom condition to stop the video
                recording = not recording
                if recording:
                    video_writer = ColorDepthWriter(output_file, DEFAULT_FPS, frame_dim, self.camera.get_intrinsics())
                else:
                    video_writer.close()
                    break

            key_pressed = cv2.waitKey(1) & 0xFF
            # if Space is pressed, toggle the pause state
            if key_pressed == ord(' '):
                is_paused = not is_paused
            # if Q is pressed, exit the loop
            elif key_pressed == ord('q'):
                break
            # if S is pressed, start recording
            elif key_pressed == ord("s"):
                recording = not recording

            # Only grab the next frame if the video is not paused
            # if N is pressed and the video is paused, grab the next frame
            if is_paused and not key_pressed == ord('n'):
                continue

            idx += 1
            self.camera.run()

            # Get the output image
            color_image = self.camera.get_rgb_img().copy()
            color_image = rotate_img(color_image, rotation_angle)

            depth_image = self.camera.get_depth_img().copy()
            depth_image = rotate_img(depth_image, rotation_angle)

            timestamp = int(self.camera.get_timestamp())
            if last_timestamp > timestamp:
                raise ValueError(f"Timestamps are not in order: {last_timestamp} > {timestamp}. Probably EOF.")
            last_timestamp = timestamp

            frame_number = self.camera.get_frame_number()
            if frame_number != last_frame_number + 1 and idx != 1:
                print(f"Frame number: {frame_number}, last frame number: {last_frame_number}")
                if recording:
                    diff = frame_number - last_frame_number - 1
                    for i in range(diff - 1):
                        video_writer.write_frames(color_image, depth_image)

            last_frame_number = frame_number

            if recording:
                video_writer.write_frames(color_image, depth_image)

            # Put some info on the image
            self.camera.put_info_to_image(color_image, idx)

            cv2.imshow("Camera", color_image)

        self.camera.close()
        video_writer.close()
        cv2.destroyAllWindows()

    def stream(self):
        rotation_angle: int = 0
        is_paused = False  # Variable to check if the video is paused or not

        idx = 0
        while True:
            key_pressed = cv2.waitKey(1) & 0xFF
            # if Space is pressed, toggle the pause state
            if key_pressed == ord(' '):
                is_paused = not is_paused
            # if Q is pressed, exit the loop
            elif key_pressed == ord('q'):
                break
            # if R is pressed, rotate the image
            elif key_pressed == ord('r'):
                rotation_angle += 90
                rotation_angle %= 360

            # Only grab the next frame if the video is not paused
            # if N is pressed and the video is paused, grab the next frame
            if is_paused and not key_pressed == ord('n'):
                continue

            idx += 1
            self.camera.run()

            # Get the output image
            output_image = self.camera.get_rgb_img().copy()
            output_image = rotate_img(output_image, rotation_angle)

            # Put some info on the image
            self.camera.put_info_to_image(output_image, idx)

            cv2.imshow("Camera", output_image)

        self.camera.close()
        cv2.destroyAllWindows()


"""
Main to run either Realsense or ZED camera.
"""
if __name__ == "__main__":
    # camera_name = "D435"
    camera_name = "D455"
    # camera_name = "ZED"

    baby = "FI"
    age = "32w"
    parent_folder = f"resources/{baby}/{age}/rgbd"
    filename = f"{camera_name}"

    ends = ".bag" if camera_name == "D455" or camera_name == "D435" else ".svo"
    input_file = f"{get_root_path()}/{parent_folder}/{filename}{ends}"
    # input_file = "/media/vojta/Elements/infant_pose_estimation/data_from_lab/real_infants/NT/27w/mocap_calibration/20240405_102420.bag"
    camera = Camera(input_file, save=False)
    # camera.stream()
    #
    # calibration_output_file = f"{get_root_path()}/output/{baby}/{age}/{camera_name}/{filename}_origin.csv"
    # camera.calibrate(calibration_output_file)
    #
    # output_video_file = f"{get_root_path()}/output/{baby}/{age}/{camera_name}/{filename}_short.avi"
    # camera.cut_rgb_stream(output_video_file, idx_from=540, idx_to=1887)

    output_file = f"{get_root_path()}/output/{baby}/{age}/{camera_name}/{filename}_short"
    camera.cut_color_depth_stream(output_file, idx_from=138902, idx_to=140297)
