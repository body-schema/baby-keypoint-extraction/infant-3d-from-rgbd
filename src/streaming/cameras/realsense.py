import os
import time

import cv2
import numpy as np
import pyrealsense2 as rs

from src.enums.defaults import REALSENSE_DEFAULT_WIDTH, REALSENSE_DEFAULT_HEIGHT, DEFAULT_FPS
from src.utils.cloud import export_numpy
from src.utils.plot_utils import time_to_str


class RealsenseCamera:
    def __init__(self, bag_file: str | None = None, save: bool = False) -> None:
        self._prepare_camera_from_config(bag_file, save)
        # Specify the alignment processing stream
        self._align = rs.align(rs.stream.color)
        self._start_time = None  # Start time of the camera
        self._color_frame = None
        self._depth_frame = None

    def _prepare_camera_from_config(self, bag_file: str | None = None, save: bool = False):
        self._pipeline = rs.pipeline()
        self._rs_config = rs.config()

        if bag_file is not None:
            if save:
                self._rs_config.enable_record_to_file(bag_file)
            else:
                assert os.path.isfile(bag_file), f"Bag file {bag_file} does not exist."
                self._rs_config.enable_device_from_file(bag_file, repeat_playback=False)

        # Configure depth and color streams
        self._rs_config.enable_stream(rs.stream.color, REALSENSE_DEFAULT_WIDTH, REALSENSE_DEFAULT_HEIGHT,
                                      rs.format.rgb8, DEFAULT_FPS)
        self._rs_config.enable_stream(rs.stream.depth, REALSENSE_DEFAULT_WIDTH, REALSENSE_DEFAULT_HEIGHT,
                                      rs.format.z16, DEFAULT_FPS)

        self._pipeline.start(self._rs_config)

        self._stream_intrin = self._pipeline.get_active_profile().get_stream(
            rs.stream.color).as_video_stream_profile().intrinsics
        self._depth_intrin = self._pipeline.get_active_profile().get_stream(
            rs.stream.depth).as_video_stream_profile().intrinsics

    def run(self) -> None:
        frames = self._pipeline.wait_for_frames()
        # Aligning images to RGB image (it is smaller than the depth one)
        aligned_frames = self._align.process(frames)
        self._color_frame = aligned_frames.get_color_frame()
        self._depth_frame = aligned_frames.get_depth_frame()

        self._start_time = time.time() if self._start_time is None else self._start_time

        # print("FRAME: ", self.get_frame_number())
        if not (self._color_frame and self._depth_frame):
            print("Could not grab frames from Realsense camera. Probably EOF reached.")
            exit()

    def deproject_pixels(self, pixels: np.ndarray, depth_image: np.ndarray) -> np.ndarray:
        """
        :param pixels: 2D array with two elements in each row is expected.
        :param depth_image: Depth image.
        :returns: Array of [x, y, z] coordinates with center at the camera.
        """
        return np.apply_along_axis(self.deproject_pixel, axis=1, arr=pixels, depth_image=depth_image)

    def deproject_pixel(self, pixel: np.ndarray, depth_image: np.ndarray | None = None) -> tuple[int, int, int]:
        """
        :param pixel: 1D array with two elements is expected.
        :param depth_image: Depth image.
        :returns: Tuple of [x, y, z] coordinates with center at the camera.
        """

        if depth_image is None:
            depth_image = self.get_depth_img().copy()

        # TODO: This needs to be checked again. Why X and Y are swapped?
        if pixel[0] < 0 or pixel[0] >= depth_image.shape[1] or pixel[1] < 0 or pixel[1] >= depth_image.shape[0]:
            return 0, 0, 0
        depth = depth_image[pixel[1], pixel[0]] / 1000
        point = rs.rs2_deproject_pixel_to_point(self._stream_intrin, pixel, depth)
        # if depth == 0:
        #     print("Could not get value from point cloud.")
        return point

    def deproject_image(self, roi=None, depth_image: np.ndarray | None = None) -> np.ndarray:
        # Deproject all pixels in the image

        if depth_image is None:
            depth_image = self.get_depth_img().copy()

        height, width = depth_image.shape
        if roi is not None:
            x, y, w, h = roi
            pixels = np.array([[i, j] for i in range(x, x + w) for j in range(y, y + h)])
        else:
            pixels = np.array([[x, y] for x in range(width) for y in range(height)])
        return self.deproject_pixels(pixels, depth_image)

    def put_info_to_image(self, output_image: np.ndarray, idx: int):
        """
       :param output_image: Image to put the information.
       :param idx: Index of the color frame from which the image is taken.
       :returns: Image with information.
       """
        font_scale = 0.5
        color = (255, 0, 0)  # BGR
        thickness = 2

        # Calculate elapsed time
        elapsed_time = self.get_elapsed_time()
        elapsed_time_str = time_to_str(elapsed_time)
        # Put the elapsed time on the image
        cv2.putText(output_image, "Time: " + elapsed_time_str, (20, 50), cv2.FONT_HERSHEY_SIMPLEX, font_scale,
                    color, thickness)
        # Put the frame number on the image
        frame_number = self.get_frame_number()
        cv2.putText(output_image, "Frame Number: " + str(frame_number), (20, 100), cv2.FONT_HERSHEY_SIMPLEX, font_scale,
                    color, thickness)
        # Put the index on the image
        cv2.putText(output_image, "Frame Index: " + str(idx), (20, 150), cv2.FONT_HERSHEY_SIMPLEX, font_scale,
                    color, thickness)
        # Put timestamp on the image
        timestamp = self.get_timestamp()
        cv2.putText(output_image, "Timestamp: " + str(timestamp), (20, 200), cv2.FONT_HERSHEY_SIMPLEX, font_scale,
                    color, thickness)

    def get_camera_name(self) -> str:
        name = self._pipeline.get_active_profile().get_device().get_info(rs.camera_info.name)
        return name.split(" ")[-1]

    def close(self) -> None:
        self._pipeline.stop()

    def get_intrinsics(self) -> rs.intrinsics:
        return self._stream_intrin

    def get_fps(self) -> int:
        return DEFAULT_FPS

    def get_timestamp(self):
        if self._color_frame is None:
            return 0
        return self._color_frame.get_timestamp()

    def get_frame_number(self):
        if self._color_frame is None:
            return 0
        return self._color_frame.get_frame_number()

    def get_elapsed_time(self) -> float:
        return time.time() - self._start_time  # Elapsed time since the camera started

    def get_color_frame(self) -> rs.frame:
        return self._color_frame

    def get_depth_frame(self) -> rs.frame:
        return self._depth_frame

    def get_rgb_img(self) -> np.ndarray:
        img = np.asanyarray(self._color_frame.get_data())
        return cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

    def get_depth_img(self) -> np.ndarray:
        return np.asanyarray(self._depth_frame.get_data())

    @staticmethod
    def get_defaults_dimensions() -> tuple[int, int]:
        return REALSENSE_DEFAULT_WIDTH, REALSENSE_DEFAULT_HEIGHT


"""
Main to run Realsense camera.
OBSOLETE: This is not used anymore. It is kept for reference.
"""
# if __name__ == "__main__":
#     camera_id = "D455"
#     parent_folder = "resources"
#     filename = f"real_syn_baby_{camera_id}"
#     bag_file = get_root_path() + "/" + parent_folder + "/" + filename + ".bag"
#     save = False
#     camera = RealsenseCamera(bag_file, save)
#
#     rotation_angle: int = 0
#     is_paused = False  # Variable to check if the video is paused or not
#
#     while True:
#         key_pressed = cv2.waitKey(1) & 0xFF
#         # if Space is pressed, toggle the pause state
#         if key_pressed == ord(' '):
#             is_paused = not is_paused
#         # if N is pressed and the video is paused, grab the next frame
#         elif key_pressed == ord('n') and is_paused:
#             camera.run()
#         # if Q is pressed, exit the loop
#         elif key_pressed == ord('q'):
#             break
#         # if R is pressed, rotate the image
#         elif key_pressed == ord('r'):
#             rotation_angle += 90
#             rotation_angle %= 360
#
#         # Get the output image
#         output_image = camera.get_rgb_img().copy()
#         output_image = rotate(output_image, rotation_angle)
#
#         # Only grab the next frame if the video is not paused
#         if not is_paused:
#             camera.run()
#
#         # Calculate elapsed time
#         elapsed_time = camera.get_elapsed_time()
#         elapsed_time_str = time_to_str(elapsed_time)
#         # Put the elapsed time on the image
#         cv2.putText(output_image, "Time: " + elapsed_time_str, (20, 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
#         cv2.imshow("Realsense Camera", output_image)
#
#     camera.close()
#     cv2.destroyAllWindows()
