import pyrealsense2 as rs
from pydantic import BaseModel


class StreamIntrinsics(BaseModel):
    coeffs: list[float]
    height: int
    width: int
    model: int
    fx: float
    fy: float
    ppx: float
    ppy: float

    @staticmethod
    def from_rs(stream_intrinsics: rs.intrinsics) -> "StreamIntrinsics":
        return StreamIntrinsics(
            coeffs=stream_intrinsics.coeffs,
            height=stream_intrinsics.height,
            width=stream_intrinsics.width,
            model=stream_intrinsics.model,
            fx=stream_intrinsics.fx,
            fy=stream_intrinsics.fy,
            ppx=stream_intrinsics.ppx,
            ppy=stream_intrinsics.ppy
        )

    def to_rs(self) -> rs.intrinsics:
        intrinsics = rs.intrinsics()
        intrinsics.coeffs = self.coeffs
        intrinsics.height = self.height
        intrinsics.width = self.width
        intrinsics.model = rs.distortion(self.model)
        intrinsics.fx = self.fx
        intrinsics.fy = self.fy
        intrinsics.ppx = self.ppx
        intrinsics.ppy = self.ppy
        return intrinsics
