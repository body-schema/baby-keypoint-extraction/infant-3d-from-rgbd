import cv2

from src.streaming.cameras.realsense import RealsenseCamera
from src.streaming.cameras.zed import ZedCamera
from src.utils.cloud import show_ply, show_numpy, export_numpy
from src.utils.path import get_root_path
from src.utils.plot_utils import rotate_img


def stream_color(camera: RealsenseCamera | ZedCamera) -> None:
    rotation_angle: int = 0
    is_paused = False  # Variable to check if the video is paused or not

    idx = 0
    while True:
        key_pressed = cv2.waitKey(1) & 0xFF
        # if Space is pressed, toggle the pause state
        if key_pressed == ord(' '):
            is_paused = not is_paused
        # if Q is pressed, exit the loop
        elif key_pressed == ord('q'):
            break
        # if R is pressed, rotate the image
        elif key_pressed == ord('r'):
            rotation_angle += 90
            rotation_angle %= 360

        # Only grab the next frame if the video is not paused
        # if N is pressed and the video is paused, grab the next frame
        if is_paused and not key_pressed == ord('n'):
            continue

        idx += 1
        camera.run()

        # Get the output image
        color_image = camera.get_rgb_img().copy()
        color_image = rotate_img(color_image, rotation_angle)

        # Get depth image
        depth_image = camera.get_depth_img().copy()
        depth_image = rotate_img(depth_image, rotation_angle)

        # if C is pressed, capture the pointcloud and store it
        if key_pressed == ord('c'):
            pc_output = f"{get_root_path()}/output/pointcloud_{camera.get_camera_name()}_{idx}.ply"

            roi = cv2.selectROI("Color Image", color_image)

            # points = camera.deproject_image()
            points = camera.deproject_image(roi, depth_image)

            export_numpy(path=pc_output, points=points)
            show_ply(pc_output, window_name="PLY")
            # show_numpy(points, window_name="Numpy")

        # Put some info on the image
        camera.put_info_to_image(color_image, idx)

        cv2.imshow("Camera", color_image)

    camera.close()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    # camera_name = "D435"
    # camera_name = "D455"
    camera_name = "ZED"

    baby = "NT"
    age = "27w"
    parent_folder = f"resources/{baby}/{age}/rgbd"
    filename = f"{camera_name}"

    ends = ".bag" if camera_name == "D455" or camera_name == "D435" else ".svo"
    input_file = f"{get_root_path()}/{parent_folder}/{filename}{ends}"

    if camera_name == "ZED":
        camera = ZedCamera(input_file, save=False, body_tracking=False, flip=True)
    else:
        camera = RealsenseCamera(input_file, save=False)

    stream_color(camera)
