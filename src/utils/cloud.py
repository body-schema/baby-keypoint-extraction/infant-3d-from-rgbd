import open3d as o3d
from open3d import io
from open3d import visualization
from open3d.geometry import PointCloud
import numpy as np
import pickle

from src.utils.path import get_root_path


def get_ply(path: str) -> PointCloud:
    cloud = io.read_point_cloud(path)  # Read point cloud
    return cloud


def show_o3d(clouds: PointCloud | list[PointCloud], window_name="Open3D", color=None):
    if isinstance(clouds, list):
        # Each point cloud is painted with a different color
        if len(clouds) == 2:
            # paint the first cloud red and the second cloud blue
            clouds[0].paint_uniform_color([1, 0, 0])
            clouds[1].paint_uniform_color([0, 0, 1])
        else:
            for i in range(len(clouds)):
                clouds[i].paint_uniform_color(np.random.rand(3))
        visualization.draw_geometries(clouds, window_name=window_name)  # Visualize point cloud
    else:
        if color is not None:
            clouds.paint_uniform_color(color)
        visualization.draw_geometries([clouds], window_name=window_name)  # Visualize point cloud


def show_numpy(points: np.ndarray, window_name="Open3D") -> None:
    show_o3d(numpy_to_o3d(points), window_name=window_name)


def show_ply(path: str, window_name="Open3D") -> None:
    cloud = get_ply(path)
    show_o3d(cloud, window_name=window_name)


def export_o3d(path: str, cloud: PointCloud) -> None:
    io.write_point_cloud(path, cloud)  # Save point cloud


def export_numpy(path: str, points: np.ndarray) -> None:
    export_o3d(path, numpy_to_o3d(points))


def numpy_to_o3d(points: np.ndarray) -> PointCloud:
    o3d_pc = o3d.geometry.PointCloud()
    o3d_pc.points = o3d.utility.Vector3dVector(points)
    return o3d_pc


def o3d_to_numpy(cloud: PointCloud) -> np.ndarray:
    return np.asarray(cloud.points)


def chamfer_dist(source: PointCloud, target: PointCloud):
    source = np.asarray(source.points)
    target = np.asarray(target.points)
    dist1 = np.min(np.sum((source[:, None] - target[None]) ** 2, axis=-1), axis=1)
    dist2 = np.min(np.sum((target[:, None] - source[None]) ** 2, axis=-1), axis=1)
    return np.mean(dist1) + np.mean(dist2)


if __name__ == "__main__":
    pcd = f"{get_root_path()}/output/photogeometry/1.85/original_pcds/d435.pcd"
    show_ply(pcd)
