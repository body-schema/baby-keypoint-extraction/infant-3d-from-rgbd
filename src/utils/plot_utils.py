import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import matplotlib

from src.utils.path import get_root_path

matplotlib.use('tkagg')


def time_to_str(elapsed_time: float) -> str:
    hours, rem = divmod(elapsed_time, 3600)
    minutes, seconds = divmod(rem, 60)
    elapsed_time_str = "{:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds)
    return elapsed_time_str


def rotate_img(img: np.ndarray, angle: int):
    """
    Rotate an image by a given angle.

    Parameters:
    - img: The input image to rotate.
    - angle: The angle in degrees to rotate the image by.

    Returns:
    - The rotated image.
    """
    if angle == 0:
        return img
    elif angle == 90:
        return cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
    elif angle == 180:
        return cv2.rotate(img, cv2.ROTATE_180)
    elif angle == 270:
        return cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
    else:
        raise ValueError(f'Invalid angle value: {angle}')


def rotate_dim(dim: tuple, angle: int):
    """
    Rotate the dimensions of an image by a given angle.

    Parameters:
    - dim: The input dimensions to rotate.
    - angle: The angle in degrees to rotate the dimensions by.

    Returns:
    - The rotated dimensions.
    """
    assert len(dim) == 2, f'Invalid dimensions: {dim}'

    if angle == 0 or angle == 180:
        return dim
    elif angle == 90 or angle == 270:
        return dim[::-1]
    else:
        raise ValueError(f'Invalid angle value: {angle}')


def plot_landmark_over_time(data, landmark_name, coordinate):
    """
    Plot the specified coordinate ('x', 'y', or 'z') of a given landmark over time.

    Parameters:
    - data: pandas DataFrame containing the landmarks data.
    - landmark_name: The name of the landmark to plot.
    - coordinate: The coordinate to plot ('x', 'y', or 'z').
    """

    # Filter the data for the specified landmark
    filtered_data = data[data['landmark'] == landmark_name]

    # Plotting
    plt.figure(figsize=(10, 6))
    plt.plot(filtered_data['timestamp'], filtered_data[coordinate], marker='o', linestyle='-',
             label=f'{coordinate} coordinate')

    plt.title(f'{landmark_name} {coordinate.upper()} Coordinate Over Time')
    plt.xlabel('Timestamp')
    plt.ylabel(f'{coordinate.upper()} Coordinate Value')
    plt.legend()
    plt.grid(True)
    plt.show()


####
# Ukaz rozdily souradnic pro landmark mezi posobejdoucimi snimky


def plot_multiple_landmark_differences_by_order(data1, data2, landmark_name, coordinates, output_path):
    """
    Plot the differences in specified coordinates (e.g., 'x', 'y', 'z') of a given landmark
    over time between two datasets, matching by row order, all on a single plot.

    Parameters:
    - data1: pandas DataFrame containing the first dataset.
    - data2: pandas DataFrame containing the second dataset.
    - landmark_name: The name of the landmark to plot the differences for.
    - coordinates: A list of coordinates to calculate the differences ('x', 'y', 'z').
    """

    # Filter the data for the specified landmark
    filtered_data1 = data1[data1['landmark'] == landmark_name].reset_index(drop=True)
    filtered_data2 = data2[data2['landmark'] == landmark_name].reset_index(drop=True)

    # Find the minimum length to ensure matching lengths for the comparison
    min_length = min(len(filtered_data1), len(filtered_data2))

    # Truncate both datasets to the minimum length
    filtered_data1 = filtered_data1.head(min_length)
    filtered_data2 = filtered_data2.head(min_length)

    plt.figure(landmark_name, figsize=(15, 10))

    # Loop through each coordinate to calculate and plot the differences
    for coordinate in coordinates:
        # Calculate the difference in the specified coordinate
        differences = filtered_data1[coordinate].values - filtered_data2[coordinate].values

        # Plotting the difference
        plt.plot(range(min_length), differences, marker='o', linestyle='-', label=f'{coordinate}')

    plt.title(f'Mediapipe vs ViTPose: Difference of {landmark_name} Coordinates')
    plt.xlabel('Timestamp')
    plt.ylabel('Coordinate Differences')
    plt.legend()
    plt.grid(True)

    # Store the plot in the output folder with the name of the landmark
    plt.savefig(output_path)
    plt.show()


if __name__ == "__main__":
    path_A = get_root_path() + "/output/ViTPoseMediapipe/36w_vitpose.csv"
    data_A = pd.read_csv(path_A)

    mp = "lite"
    path_B = get_root_path() + "/output/ViTPoseMediapipe/mp_coco17_" + mp + "_36w.csv"
    data_B = pd.read_csv(path_B)

    landmark_name = 'LEFT_HIP'
    output_path = get_root_path() + "/output" + f'/{landmark_name}_mp_' + mp + '_vitpose.png'

    coordinates = ['image_x', 'image_y']
    # Example usage
    plot_multiple_landmark_differences_by_order(data_A, data_B, landmark_name, coordinates, output_path)
