import pandas as pd

from src.detection.mediapipe_pose_detector import MPModel
from src.utils.path import get_root_path


def up_sample(camera_file: str, up_sampled_file: str, in_fps: int = 29, camera_fps: int = 30):
    # Load the CSV file
    data = pd.read_csv(camera_file)

    # Initialize an empty DataFrame to store the up-sampled data
    upsampled_data = pd.DataFrame()

    # Calculate the number of extra frames to add in each buffer
    extra_frames = camera_fps - in_fps

    # Process frames in batches of in_FPS, adding extra_frames to each batch
    for start in range(0, len(data), in_fps):
        shift = min((start + in_fps), len(data))
        batch = data.iloc[start:shift]

        # Duplicate the first extra_frames frames in the batch
        duplicated_frames = batch.iloc[:extra_frames]

        # Concatenate the duplicated frames with the batch
        upsampled_batch = pd.concat([batch, duplicated_frames], ignore_index=True)

        # Add the upsampled batch to the upsampled data
        upsampled_data = pd.concat([upsampled_data, upsampled_batch], ignore_index=True)

    # Save the up-sampled data to a new CSV file
    upsampled_data.to_csv(up_sampled_file, index=False)


if __name__ == "__main__":
    baby = "NT"
    age = "27w"
    model = MPModel.LITE
    parent_folder = f"{get_root_path()}/output/{baby}/{age}/ZED/{model}"

    in_file = f"{parent_folder}/ZED_short_{model}_diff.csv"
    out_file = f"{parent_folder}/ZED_short_{model}_diff_upsampled.csv"

    # Up-sample the data
    up_sample(in_file, out_file)
