import numpy as np
import pandas as pd

from src.utils.path import get_root_path


def down_sample(mocap_file: str, down_sampled_file: str, mocap_fps: int = 100, camera_fps: int = 30):
    # Load the CSV file
    data = pd.read_csv(mocap_file)

    # Initialize an empty DataFrame to store the down-sampled data
    downsampled_data = pd.DataFrame()

    # Process frames in batches of mocap_FPS, selecting camera_FPS frames from each
    for start in range(0, len(data), mocap_fps):
        shift = min((start + mocap_fps), len(data))
        batch = data.iloc[start:shift]
        indices = np.linspace(0, len(batch) - 1, camera_fps,
                              dtype=int)  # Calculate indices of camera_FPS evenly spaced frames
        selected_frames = batch.iloc[indices]
        downsampled_data = pd.concat([downsampled_data, selected_frames], ignore_index=True)

    # Save the down-sampled data to a new CSV file
    downsampled_data.to_csv(down_sampled_file, index=False)


if __name__ == "__main__":
    baby = "NT"
    age = "27w"
    parent_folder = f"{get_root_path()}/resources/{baby}/{age}/mocap"

    prefix = "good"
    mocap_file = f"{parent_folder}/{prefix}_part.csv"

    # Down-sample the data
    down_sample(mocap_file, f"{parent_folder}/{prefix}_part_downsampled.csv")
