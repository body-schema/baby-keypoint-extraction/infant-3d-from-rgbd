import os
import sys


# return path to the root of the project
def get_root_path():
    return os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
