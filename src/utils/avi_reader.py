# Create an AVI reader, which reads AVI file frame by frame and can be paused and resumed with SPACE key.

import cv2
from src.utils.path import get_root_path
from src.utils.video_writer import VideoWriter


class AVIReader:
    def __init__(self, input_file: str, output_file: str):
        self.input_file = input_file
        self.output_file = output_file
        self.cap = cv2.VideoCapture(input_file)
        self.frame_dim = (
            int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
            int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT)),
        )
        self.fps = self.cap.get(cv2.CAP_PROP_FPS)

        self.output_video = None

        print(f"Frame dimensions: {self.frame_dim}")
        print(f"FPS: {self.fps}")

    def run(self, idx_from: int = 0, idx_to: int = 0):
        idx: int = 0
        is_paused = False  # Variable to check if the video is paused or not
        recording = False

        while True:
            if idx_to != 0 and (idx == idx_from or idx == idx_to):  # My custom condition to stop the video
                recording = not recording
                if recording:
                    self.output_video = VideoWriter(self.output_file, self.fps, self.frame_dim)
                else:
                    self.output_video.close()

            key_pressed = cv2.waitKey(1) & 0xFF
            # if Space is pressed, toggle the pause state
            if key_pressed == ord(" "):
                is_paused = not is_paused
            # if Q is pressed, exit the loop
            elif key_pressed == ord("q"):
                break
            # if S is pressed, start recording
            elif key_pressed == ord("s"):
                recording = not recording

            # Only grab the next frame if the video is not paused
            # if N is pressed and the video is paused, grab the next frame
            if is_paused and not key_pressed == ord("n"):
                continue

            ret, frame = self.cap.read()
            if not ret:
                break
            idx += 1

            if recording:
                self.output_video.write_frame(frame)

            # Display also the index of the frame
            cv2.putText(
                frame,
                f"Frame: {idx}",
                (10, 30),
                cv2.FONT_HERSHEY_SIMPLEX,
                1,
                (0, 255, 0),
                2,
                cv2.LINE_AA,
            )

            # Display the frame
            cv2.imshow("Frame", frame)

        self.cap.release()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    avi_in = "nt_Miqus_5_23913.avi"
    avi_out = "nt_Miqus_5_23913_short.avi"
    input_path = f"{get_root_path()}/resources/NT/27w/{avi_in}"
    output_path = f"{get_root_path()}/output/NT/27w/{avi_out}"

    reader = AVIReader(input_path, output_path)
    reader.run(7212, 7750)
