from typing import List

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pydantic import BaseModel

from src.detection.mediapipe_pose_detector import MPModel
from src.enums.mp_enum import MPPoseLandmark
from src.utils.path import get_root_path


class PlotData(BaseModel):
    data: List[float]
    label: str
    color: str = 'b'
    marker: str = 'o'


def visualize_data_axis_specific(data: [PlotData], lm=MPPoseLandmark.LEFT_ANKLE, axis='z'):
    # Plot both hips in 2D
    for d in data:
        plt.plot(d.data, marker=d.marker, color=d.color, label=d.label)
    plt.title(f'{lm.name} Landmark in 2D')
    plt.xlabel('Frame')
    plt.ylabel(f'{axis.capitalize()} in mm')
    plt.legend()
    plt.show()


def rgbd_rgbd_diff(rgbd_data: str, rgbd_data2: str, lm=MPPoseLandmark.LEFT_HIP, axis='z', axis2='z'):
    rgbd_df = pd.read_csv(rgbd_data)
    rgbd_lm = rgbd_df[rgbd_df['landmark'] == lm.name]
    gain = -1000 if axis == 'z' else 1000  # Multiply by 1000 to convert meters to millimeters
    rgbd_lm.loc[:, axis] = rgbd_lm[axis] * gain
    # extract z axis to a list
    shift = 180
    rgbd_z_axis = rgbd_lm[axis].tolist()[shift:]

    rgbd_df2 = pd.read_csv(rgbd_data2)
    rgbd_lm2 = rgbd_df2[rgbd_df2['landmark'] == lm.name]
    rgbd_lm2.loc[:, axis2] = rgbd_lm2[axis2] * gain
    # extract z axis to a list
    rgbd_z_axis2 = rgbd_lm2[axis2].tolist()[shift:]

    plot_data = [
        PlotData(data=rgbd_z_axis, label=f'{rgbd_data.split("/")[-3]}_{axis}', color='b'),
        PlotData(data=rgbd_z_axis2, label=f'{rgbd_data2.split("/")[-3]}_{axis2}', color='g')
    ]

    visualize_data_axis_specific(plot_data, lm, axis)


def rgbd_rgbd_mocap_diff(rgbd_data: str, rgbd_data2: str, mocap_data: str, lm=MPPoseLandmark.LEFT_ANKLE, rgbd_axis='x',
                         rgbd_axis2='model_x', mocap_axis='y'):
    rgbd_df = pd.read_csv(rgbd_data)
    rgbd_lm = rgbd_df[rgbd_df['landmark'] == lm.name]
    gain = -1000 if rgbd_axis == 'z' else 1000  # Multiply by 1000 to convert meters to millimeters
    rgbd_lm.loc[:, rgbd_axis] = rgbd_lm[rgbd_axis] * gain
    # extract axis to a list
    shift = 180
    rgbd_ax = rgbd_lm[rgbd_axis].tolist()[shift:]

    rgbd_df2 = pd.read_csv(rgbd_data2)
    rgbd_lm2 = rgbd_df2[rgbd_df2['landmark'] == lm.name]
    gain = -1000 if rgbd_axis2 == 'z' else 1000  # Multiply by 1000 to convert meters to millimeters
    rgbd_lm2.loc[:, rgbd_axis2] = rgbd_lm2[rgbd_axis2] * gain
    # extract axis to a list
    rgbd_ax2 = rgbd_lm2[rgbd_axis2].tolist()[shift:]

    mocap_df = pd.read_csv(mocap_data)[f'{lm.to_mocap()}_{mocap_axis}'].tolist()

    plot_data = [
        PlotData(data=rgbd_ax, label=f'{rgbd_data.split("/")[-3]}_{rgbd_axis}', color='b'),
        PlotData(data=rgbd_ax2, label=f'{rgbd_data2.split("/")[-3]}_{rgbd_axis2}', color='g'),
        PlotData(data=mocap_df, label=f'MoCap_{mocap_axis}', color='r')
    ]

    visualize_data_axis_specific(plot_data, lm, mocap_axis)


def zed_zed_mocap_diff(rgbd_data: str, rgbd_data2: str, mocap_data: str, lm=MPPoseLandmark.LEFT_ANKLE, rgbd_axis='z',
                       rgbd_axis2='z', mocap_axis='z'):
    rgbd_df = pd.read_csv(rgbd_data)
    rgbd_lm = rgbd_df[rgbd_df['landmark'] == lm.name]
    gain = 1000
    rgbd_lm.loc[:, rgbd_axis] = rgbd_lm[rgbd_axis] * gain
    # extract axis to a list
    shift = 0
    rgbd_ax = rgbd_lm[rgbd_axis].tolist()[shift:]

    rgbd_df2 = pd.read_csv(rgbd_data2)
    rgbd_lm2 = rgbd_df2[rgbd_df2['landmark'] == lm.name]
    gain = 1000
    rgbd_lm2.loc[:, rgbd_axis2] = rgbd_lm2[rgbd_axis2] * gain
    # extract axis to a list
    rgbd_ax2 = rgbd_lm2[rgbd_axis2].tolist()[shift:]

    shift = 40
    mocap_df = pd.read_csv(mocap_data)[f'{lm.to_mocap()}_{mocap_axis}'].tolist()[shift:]
    plot_data = [
        PlotData(data=rgbd_ax, label=f'{rgbd_data.split("/")[-3]}_{rgbd_axis}', color='b'),
        PlotData(data=rgbd_ax2, label=f'{rgbd_data2.split("/")[-3]}_{rgbd_axis2}', color='g'),
        PlotData(data=mocap_df, label=f'MoCap_{mocap_axis}', color='r')
    ]

    visualize_data_axis_specific(plot_data, lm, mocap_axis)


def zed_mocap(rgbd_data: str, mocap_data: str, lm=MPPoseLandmark.LEFT_ANKLE, rgbd_axis='x', mocap_axis='y'):
    rgbd_df = pd.read_csv(rgbd_data)
    rgbd_lm = rgbd_df[rgbd_df['landmark'] == lm.name]
    gain = 1000 if rgbd_axis == 'z' else -1000 if rgbd_axis == 'y' else 1000
    rgbd_lm.loc[:, rgbd_axis] = rgbd_lm[rgbd_axis] * gain
    # extract axis to a list
    shift = 0
    rgbd_ax = rgbd_lm[rgbd_axis].tolist()[shift:]

    shift = 32
    mocap_df = pd.read_csv(mocap_data)[f'{lm.to_mocap()}_{mocap_axis}'].tolist()[shift:]
    plot_data = [
        PlotData(data=rgbd_ax, label=f'{rgbd_data.split("/")[-3]}_{rgbd_axis}', color='b'),
        PlotData(data=mocap_df, label=f'MoCap_{mocap_axis}', color='r')
    ]

    visualize_data_axis_specific(plot_data, lm, mocap_axis)


def rgbd_mocap_diff(rgbd_data: str, mocap_data: str, lm=MPPoseLandmark.LEFT_ANKLE, rgbd_axis='x', mocap_axis='y'):
    rgbd_df = pd.read_csv(rgbd_data)
    rgbd_lm = rgbd_df[rgbd_df['landmark'] == lm.name]
    gain = -1000 if rgbd_axis == 'z' else 1000  # Multiply by 1000 to convert meters to millimeters
    rgbd_lm.loc[:, rgbd_axis] = rgbd_lm[rgbd_axis] * gain
    # extract z axis to a list
    shift = 0
    rgbd_z_axis = rgbd_lm[rgbd_axis].tolist()[shift:]

    mocap_df = pd.read_csv(mocap_data)[f'{lm.to_mocap()}_{mocap_axis}'].tolist()

    plot_data = [
        PlotData(data=rgbd_z_axis, label=f'{rgbd_data.split("/")[-3]}_{rgbd_axis}', color='b'),
        PlotData(data=mocap_df, label=f'MoCap_{mocap_axis}', color='r')
    ]

    visualize_data_axis_specific(plot_data, lm, mocap_axis)


def extract_all_differences(rgbd_data: str, mocap_data: str, lm=MPPoseLandmark.LEFT_ANKLE):
    columns = ['x', 'y', 'z']
    rgbd_df = pd.read_csv(rgbd_data)
    left_hip_rgbd = rgbd_df[rgbd_df['landmark'] == lm.name][columns]
    # Multiply by 1000 to convert meters to millimeters
    left_hip_rgbd = left_hip_rgbd * 1000
    # Multiply by -1 to invert the Z axis
    left_hip_rgbd['z'] = left_hip_rgbd['z'] * -1

    columns = [f'{lm.to_mocap()}_x', f'{lm.to_mocap()}_y', f'{lm.to_mocap()}_z']
    mocap_df = pd.read_csv(mocap_data)
    left_hip_mocap = mocap_df[columns]

    # Plot both hips in 3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(left_hip_rgbd['y'], left_hip_rgbd['x'], left_hip_rgbd['z'], label='RGBD')
    ax.scatter(left_hip_mocap[f'{lm.to_mocap()}_x'], left_hip_mocap[f'{lm.to_mocap()}_y'],
               left_hip_mocap[f'{lm.to_mocap()}_z'], label='MoCap')
    ax.set_title(f'{lm.name} Landmark in 3D')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    ax.legend()
    plt.show()


def visualize_rgbd_axis(rgbd_file: str, lm=MPPoseLandmark.RIGHT_ANKLE, axis='y'):
    # Load the CSV file
    data = pd.read_csv(rgbd_file)
    axis_data = data[data['landmark'] == lm.name][axis].tolist()
    # remove zeros
    axis_data = [x for x in axis_data if x != 0]

    plt.plot(axis_data, marker='o', color='b', label='RGBD')
    plt.title(f'{lm.name} Landmark in 2D')
    plt.xlabel('Frame')
    plt.ylabel(f'{axis.capitalize()} in mm')
    plt.legend()
    plt.show()


def visualize_mocap_axis(mocap_file: str, lm=MPPoseLandmark.LEFT_ANKLE, axis='z'):
    # Load the CSV file
    data = pd.read_csv(mocap_file)

    plt.plot(data[f'{lm.to_mocap()}_{axis}'], marker='o', color='r', label='MoCap')
    plt.title(f'{lm.name} Landmark in 2D')
    plt.xlabel('Frame')
    plt.ylabel(f'{axis.capitalize()} in mm')
    plt.legend()
    plt.show()


def visualize_mocap_3d_skeleton(mocap_file: str):
    # Load the CSV file
    data = pd.read_csv(mocap_file)

    # Define the connections (edges) between landmarks
    # Assume indices are ordered as: Shoulder, Elbow, Wrist, Hip, Knee, Ankle
    # This will be updated based on your specific data column names or order
    edges = [[("LEFT_EAR", "LEFT_EYE"), ("LEFT_EYE", "NOSE"), ("NOSE", "RIGHT_EYE"), ("RIGHT_EYE", "RIGHT_EAR")],
             # Face
             [("LEFT_SHOULDER", "LEFT_ELBOW"), ("LEFT_ELBOW", "LEFT_WRIST")],  # Left arm
             [("RIGHT_SHOULDER", "RIGHT_ELBOW"), ("RIGHT_ELBOW", "RIGHT_WRIST")],  # Right arm
             [("LEFT_HIP", "LEFT_KNEE"), ("LEFT_KNEE", "LEFT_ANKLE")],  # Left leg
             [("RIGHT_HIP", "RIGHT_KNEE"), ("RIGHT_KNEE", "RIGHT_ANKLE")]]  # Right leg

    # Unique frame indices
    frame_indices = data['frame_idx'].unique()

    # Create a 3D plot for each frame
    for frame_idx in frame_indices:
        frame_data = data[data['frame_idx'] == frame_idx]
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        # Plot each landmark as a scatter plot, however, do not show the landmarks with zero coordinates
        for idx, landmark in frame_data.iterrows():
            if np.all(landmark[['x', 'y', 'z']].values == 0):
                continue
            ax.scatter(landmark['x'], landmark['y'], landmark['z'])
            # ax.scatter(landmark['x'], landmark['y'], landmark['z'], label=landmark['landmark'])
        # ax.scatter(frame_data['x'], frame_data['y'], frame_data['z'], label=f'Frame {frame_idx}')

        # Draw lines between connected landmarks
        for limb in edges:
            for edge in limb:
                from_idx, to_idx = edge
                from_landmark = frame_data[frame_data['landmark'] == from_idx]
                to_landmark = frame_data[frame_data['landmark'] == to_idx]

                # If one of the landmark has zero coordinates, skip this edge
                if np.all(from_landmark[['x', 'y', 'z']].values == 0) or np.all(
                        to_landmark[['x', 'y', 'z']].values == 0):
                    continue

                ax.plot([from_landmark['x'], to_landmark['x']],
                        [from_landmark['y'], to_landmark['y']],
                        [from_landmark['z'], to_landmark['z']], color='b')

        ax.set_title(f'Landmarks for Frame {frame_idx}')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        ax.legend()
        plt.show()


if __name__ == "__main__":
    baby = "FI"
    age = "32w"
    parent_folder = f"{get_root_path()}/resources/{baby}/{age}/mocap"

    model = MPModel.LITE
    rgbd_file = f"{get_root_path()}/output/{baby}/{age}/D435/{model}/D435_short_{model}_diff.csv"
    rgbd_file2 = f"{get_root_path()}/output/{baby}/{age}/D455/{model}/D455_short_{model}_diff.csv"
    rgbd_file3 = f"{get_root_path()}/output/{baby}/{age}/ZED/{model}/ZED_short_{model}_diff.csv"
    rgbd_file4 = f"{get_root_path()}/output/{baby}/{age}/ZED/native/ZED_short_native_diff.csv"

    part = "first"
    # part = "second"
    mocap_file = f"{parent_folder}/{part}_part_downsampled.csv"

    # extract_all_differences(rgbd_file, mocap_file)
    # show_axis_diff(rgbd_file, mocap_file)

    # rgbd_mocap_diff(rgbd_file, mocap_file)
    # rgbd_rgbd_diff(rgbd_file, rgbd_file2)
    # rgbd_rgbd_mocap_diff(rgbd_file, rgbd_file2, mocap_file)
    # zed_zed_mocap_diff(rgbd_file3, rgbd_file4, mocap_file)
    zed_mocap(rgbd_file4, mocap_file)

    # evaluate_everything(rgbd_file, rgbd_file2, mocap_file)
