import open3d as o3d
import copy
import numpy as np
import pickle as pkl
from open3d.geometry import PointCloud

from src.utils.cloud import show_ply, show_o3d, chamfer_dist
from src.utils.path import get_root_path


def draw_registration_result(source, target, transformation, color=True):
    source_temp = copy.deepcopy(source)
    target_temp = copy.deepcopy(target)
    if color:
        source_temp.paint_uniform_color([1, 0, 0])
        target_temp.paint_uniform_color([0, 0, 1])
    source_temp.transform(transformation)
    o3d.visualization.draw_geometries([target_temp, source_temp])


def preprocess_point_cloud(pcd, voxel_size):
    pcd_down = pcd.voxel_down_sample(voxel_size)

    radius_normal = voxel_size * 2
    pcd_down.estimate_normals(
        o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))

    radius_feature = voxel_size * 5
    pcd_fpfh = o3d.pipelines.registration.compute_fpfh_feature(
        pcd_down,
        o3d.geometry.KDTreeSearchParamHybrid(radius=radius_feature, max_nn=100))
    return pcd_down, pcd_fpfh


def execute_global_registration(source_down, target_down, source_fpfh,
                                target_fpfh, voxel_size):
    distance_threshold = voxel_size * 1.5
    result = o3d.pipelines.registration.registration_ransac_based_on_feature_matching(
        source_down, target_down, source_fpfh, target_fpfh, True,
        distance_threshold,
        o3d.pipelines.registration.TransformationEstimationPointToPoint(False),
        3, [
            o3d.pipelines.registration.CorrespondenceCheckerBasedOnEdgeLength(
                0.9),
            o3d.pipelines.registration.CorrespondenceCheckerBasedOnDistance(
                distance_threshold)
        ], o3d.pipelines.registration.RANSACConvergenceCriteria(100000, 0.999))
    return result


class Results:
    def __init__(self, reg_results):
        for attr in [_ for _ in reg_results.__dir__() if _[0] != "_"]:
            if "correspondence_set" in attr:
                setattr(self, attr, np.asarray(getattr(reg_results, attr)))
            else:
                setattr(self, attr, getattr(reg_results, attr))


if __name__ == "__main__":
    cams = ["d435", "d455", "zed"]
    heights = ["1.25", "1.85"]

    # cams = ["d455"]
    # heights = ["1.85"]
    for cam in cams:
        for height in heights:
            threshold = 0.01
            if cam == "zed" and height == "1.85":
                threshold = 0.2
            voxel_size = 0.01

            root = f"{get_root_path()}/output/photogeometry/{height}"
            source = o3d.io.read_point_cloud(f"{root}/original_pcds/{cam}.pcd")
            target = o3d.io.read_point_cloud(f"{root}/original_pcds/fg.pcd")
            source_down, source_fpfh = preprocess_point_cloud(source, voxel_size)
            target_down, target_fpfh = preprocess_point_cloud(target, voxel_size)

            # show downsampled point clouds
            # show_o3d(source, window_name=f"{cam} Original")
            # show_o3d(source_down, window_name=f"{cam} Downsampled", color=[1,0,0])
            # show_o3d(target, window_name="FG Original")
            # show_o3d(target_down, window_name="FG Downsampled", color=[0,0,1])

            result_ransac = execute_global_registration(source_down, target_down,
                                                        source_fpfh, target_fpfh,
                                                        voxel_size)
            # draw_registration_result(source_down, target_down, result_ransac.transformation)
            # draw_registration_result(source, target, result_ransac.transformation)

            #         draw_registration_result(source_down, target_down, result_ransac.transformation)
            reg_p2p = o3d.pipelines.registration.registration_icp(
                source_down, target_down, threshold, result_ransac.transformation,
                o3d.pipelines.registration.TransformationEstimationPointToPoint())
            with open(f"{root}/registrations_info/{cam}.pkl", "wb") as f:
                pkl.dump(Results(reg_p2p), f)

            print(cam)
            print(height)
            print(reg_p2p)

            source_down.transform(reg_p2p.transformation)
            cd = chamfer_dist(source_down, target_down)
            print(f"Chamfer Distance: {cd:.7f}")

            # draw_registration_result(source_down, target_down, reg_p2p.transformation)
            # draw_registration_result(source, target, reg_p2p.transformation)

            source.transform(reg_p2p.transformation)
            o3d.io.write_point_cloud(f"{root}/transformed_pcds/{cam}.pcd", source)
