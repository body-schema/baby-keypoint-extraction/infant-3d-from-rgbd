import numpy as np
from mediapipe.tasks.python.components.containers.landmark import Landmark, NormalizedLandmark

from src.enums.coco_enum import CoCo
from src.enums.mp_enum import MPPoseLandmark
import os
import json
import csv
import glob

from src.enums.zed_enum import ZedPoseLandmark
from src.utils.path import get_root_path


class CSVWriter:
    def __init__(self, file_path: str):
        self.file_path = file_path
        self.file = open(file_path, "w")
        self.file.write(
            "frame_idx,frame_number,timestamp,landmark,landmark_id,image_x,image_y,model_x,model_y,model_z,"
            "presence,visibility,x,y,z\n")
        self.file.close()

    def write(self, frame_idx: int, frame_number: int, timestamp: int, landmark_xyz: Landmark | tuple[int, int, int],
              landmark_xy: NormalizedLandmark | tuple[int, int],
              world_coordinates: tuple[int, int, int] | np.ndarray,
              landmark: MPPoseLandmark | ZedPoseLandmark | CoCo | str):
        assert len(world_coordinates) == 3, "World coordinates must have 3 elements."
        self.file = open(self.file_path, "a")
        if isinstance(landmark, MPPoseLandmark):
            self.file.write(
                f"{frame_idx},{frame_number},{timestamp},{landmark.name},{landmark.value},"
                f"{landmark_xy.x},{landmark_xy.y},"
                f"{landmark_xyz.x},{landmark_xyz.y},{landmark_xyz.z},{landmark_xyz.presence},{landmark_xyz.visibility},"
                f"{world_coordinates[0]},{world_coordinates[1]},{world_coordinates[2]}\n")
        elif isinstance(landmark, ZedPoseLandmark):
            self.file.write(
                f"{frame_idx},{frame_number},{timestamp},{landmark.name},{landmark.value},"
                f"{landmark_xy[0]},{landmark_xy[1]},"
                f"{landmark_xyz[0]},{landmark_xyz[1]},{landmark_xyz[2]},Nan,Nan,"
                f"{world_coordinates[0]},{world_coordinates[1]},{world_coordinates[2]}\n")
        elif isinstance(landmark, CoCo):
            self.file.write(
                f"{frame_idx},{frame_number},{timestamp},{landmark.name},{landmark.value},"
                f"{landmark_xy[0]},{landmark_xy[1]},"
                f"0,0,0,0,0,"
                f"{world_coordinates[0]},{world_coordinates[1]},{world_coordinates[2]}\n")
        elif isinstance(landmark, str):
            # Probably just for simple writing of some point
            self.file.write(
                f"{frame_idx},{frame_number},{timestamp},{landmark},0,"  # frame_idx, frame_number, timestamp, landmark, landmark_id
                f"{landmark_xy[0]},{landmark_xy[1]},"  # image_x, image_y
                f"Nan,Nan,Nan,Nan,Nan,"  # model_x, model_y, model_z, presence, visibility
                f"{world_coordinates[0]},{world_coordinates[1]},{world_coordinates[2]}\n")  # x, y, z
        self.file.close()

    def from_json(self, json_dir: str) -> None:
        # Use glob to find all JSON files in the directory
        json_files = glob.glob(os.path.join(json_dir, '*.json'))

        # Sort the files based on the numerical timestamp in the file name
        json_files = sorted(json_files, key=lambda x: int(os.path.splitext(os.path.basename(x))[0].split('_')[-1]))

        # Prepare to write to the CSV file
        self.file = open(self.file_path, "a")

        # Iterate over each JSON file
        for json_file in json_files:
            # Extract the timestamp from the file name
            timestamp = os.path.splitext(os.path.basename(json_file))[0].split('_')[-1]

            # Load the JSON data
            with open(json_file) as f:
                data = json.load(f)

            # Assume the keypoints are stored under the 'keypoints' key for each numbered key in order
            # You might need to adjust this depending on the actual structure
            for _, person_data in data.items():  # Adjust this loop depending on your JSON structure
                keypoints = person_data['keypoints']
                # split keypoints into x, y, score
                keypoints = [keypoints[i:i + 3] for i in range(0, len(keypoints), 3)]

                for i, keypoint in enumerate(keypoints):
                    self.file.write(
                        f"{timestamp},{CoCo(i).name},{i},"  # timestamp, landmark, landmark_id
                        f"{keypoint[0]},{keypoint[1]},"  # x, y
                        f"{0},{0},{0},"  # model x, model y, model z
                        f"{keypoint[2]},{0},"  # presence and visibility
                        f"{0},{0},{0}\n")  # x, y, z

                # Process just only the first found person in the frame
                break


if __name__ == "__main__":
    output_csv = get_root_path() + "/output/36w_vitpose.csv"
    csv_writer = CSVWriter(output_csv)
    csv_writer.from_json("/media/vojta/Elements/infant_pose_estimation/data_from_lab/mmpose_td/KW/keypoints_coco17")
