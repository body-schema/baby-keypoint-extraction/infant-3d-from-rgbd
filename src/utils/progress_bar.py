
def progress_bar(iteration, total):
    percent = 100 * (iteration / float(total))
    bar = '#' * int(percent) + '-' * (100 - int(percent))
    print(f"\r|{bar}| {percent:.2f}%", end="\r")
    if iteration == total:
        print()
