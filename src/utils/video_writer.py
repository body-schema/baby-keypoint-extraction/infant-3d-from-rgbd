import cv2
import numpy as np
import pyrealsense2 as rs

from src.streaming.cameras.config import StreamIntrinsics


class VideoWriter:
    def __init__(self, output_file, fps, frame_size):
        self.writer = cv2.VideoWriter(
            output_file,
            apiPreference=cv2.CAP_FFMPEG,
            fourcc=cv2.VideoWriter_fourcc(*"mp4v"),
            fps=fps,
            frameSize=frame_size,
        )

    def write_frame(self, frame: np.ndarray):
        self.writer.write(frame.astype("uint8"))

    def close(self):
        self.writer.release()


class DepthWriter:
    def __init__(self, output_file, fps, frame_size):
        self.writer = cv2.VideoWriter(
            output_file,
            apiPreference=cv2.CAP_FFMPEG,
            fourcc=cv2.VideoWriter_fourcc(*"FFV1"),
            fps=fps,
            frameSize=frame_size,
            params=[
                cv2.VIDEOWRITER_PROP_DEPTH,
                cv2.CV_16U,
                cv2.VIDEOWRITER_PROP_IS_COLOR,
                0,
            ]
        )

    def write_frame(self, frame: np.ndarray):
        self.writer.write(frame.astype("uint16"))

    def close(self):
        self.writer.release()


class ColorDepthWriter:
    def __init__(self, output_file, fps, frame_size, intrinsics: rs.intrinsics):
        self._color_writer = cv2.VideoWriter(
            output_file + "_color.mp4",
            apiPreference=cv2.CAP_FFMPEG,
            fourcc=cv2.VideoWriter_fourcc(*"mp4v"),
            fps=fps,
            frameSize=frame_size,
        )
        self._depth_writer = cv2.VideoWriter(
            output_file + "_depth.mkv",
            apiPreference=cv2.CAP_FFMPEG,
            fourcc=cv2.VideoWriter_fourcc(*"FFV1"),
            fps=fps,
            frameSize=frame_size,
            params=[
                cv2.VIDEOWRITER_PROP_DEPTH,
                cv2.CV_16U,
                cv2.VIDEOWRITER_PROP_IS_COLOR,
                0,
            ]
        )
        self._stream_intrinsics = StreamIntrinsics.from_rs(intrinsics)
        # Save the intrinsics to a JSON file
        intrin_file = open(output_file + "_intrinsics.json", "w")
        intrin_file.write(self._stream_intrinsics.model_dump_json(indent=4))

    def write_frames(self, color_frame: np.ndarray, depth_frame: np.ndarray):
        self._color_writer.write(color_frame.astype("uint8"))
        self._depth_writer.write(depth_frame.astype("uint16"))

    def close(self):
        self._color_writer.release()
        self._depth_writer.release()


class ColorDepthReader:
    def __init__(self, input_file: str):
        self._color_reader = cv2.VideoCapture(
            input_file + "_color.mp4",
            apiPreference=cv2.CAP_FFMPEG
        )
        self._depth_reader = cv2.VideoCapture(
            input_file + "_depth.mkv",
            apiPreference=cv2.CAP_FFMPEG,
            params=[
                cv2.CAP_PROP_CONVERT_RGB,
                0,
            ]
        )
        intrin_file = open(input_file + "_intrinsics.json", "r")
        self._stream_intrinsics: rs.intrinsics = StreamIntrinsics.model_validate_json(intrin_file.read()).to_rs()

        self._info()

    def _info(self):
        # Print the amount of frames in the video
        print("Color frames:", self._color_reader.get(cv2.CAP_PROP_FRAME_COUNT))
        print("Depth frames:", self._depth_reader.get(cv2.CAP_PROP_FRAME_COUNT))
        # Print out FPS
        print("Color FPS:", self._color_reader.get(cv2.CAP_PROP_FPS))
        print("Depth FPS:", self._depth_reader.get(cv2.CAP_PROP_FPS))

    def get_frame_size(self):
        return (
            int(self._color_reader.get(cv2.CAP_PROP_FRAME_WIDTH)),
            int(self._color_reader.get(cv2.CAP_PROP_FRAME_HEIGHT))
        )

    def get_fps(self):
        return self._color_reader.get(cv2.CAP_PROP_FPS)

    @property
    def intrinsics(self) -> rs.intrinsics:
        return self._stream_intrinsics

    def get_frames(self):
        ret_color, color_image = self._color_reader.read()
        ret_depth, depth_image = self._depth_reader.read()
        if color_image is None or depth_image is None:
            return False, None, False, None
        return ret_color, color_image, ret_depth, depth_image

    def get_timestamp(self):
        return self._color_reader.get(cv2.CAP_PROP_POS_MSEC)

    def close(self):
        self._color_reader.release()
        self._depth_reader.release()


class ColorReader:
    def __init__(self, input_file: str):
        self._color_stream = cv2.VideoCapture(input_file)
        self._info()

    def _info(self):
        # Print the amount of frames in the video
        print("Color frames:", self._color_stream.get(cv2.CAP_PROP_FRAME_COUNT))
        # Print out FPS
        print("Color FPS:", self._color_stream.get(cv2.CAP_PROP_FPS))

    def get_frame_size(self):
        return (
            int(self._color_stream.get(cv2.CAP_PROP_FRAME_WIDTH)),
            int(self._color_stream.get(cv2.CAP_PROP_FRAME_HEIGHT))
        )

    def get_fps(self):
        return self._color_stream.get(cv2.CAP_PROP_FPS)

    def get_frames(self):
        ret_color, color_image = self._color_stream.read()
        if color_image is None:
            return False, None
        return ret_color, color_image.copy()

    def get_timestamp(self):
        return self._color_stream.get(cv2.CAP_PROP_POS_MSEC)

    def close(self):
        self._color_stream.release()


class DepthReader:
    def __init__(self, input_file: str):
        self._depth_reader = cv2.VideoCapture(
            input_file,
            apiPreference=cv2.CAP_FFMPEG,
            params=[
                cv2.CAP_PROP_CONVERT_RGB,
                0,
            ]
        )
        intrin_file = open(input_file + "_intrinsics.json", "r")
        self._stream_intrinsics: rs.intrinsics = StreamIntrinsics.model_validate_json(intrin_file.read()).to_rs()

        self._info()

    def _info(self):
        # Print the amount of frames in the video
        print("Depth frames:", self._depth_reader.get(cv2.CAP_PROP_FRAME_COUNT))
        # Print out FPS
        print("Depth FPS:", self._depth_reader.get(cv2.CAP_PROP_FPS))

    def get_frame_size(self):
        return (
            int(self._depth_reader.get(cv2.CAP_PROP_FRAME_WIDTH)),
            int(self._depth_reader.get(cv2.CAP_PROP_FRAME_HEIGHT))
        )

    def get_fps(self):
        return self._depth_reader.get(cv2.CAP_PROP_FPS)

    @property
    def intrinsics(self) -> rs.intrinsics:
        return self._stream_intrinsics

    def get_frames(self):
        ret_depth, depth_image = self._depth_reader.read()
        if depth_image is None:
            return False, None
        return ret_depth, depth_image

    def get_timestamp(self):
        return self._depth_reader.get(cv2.CAP_PROP_POS_MSEC)

    def close(self):
        self._depth_reader.release()
