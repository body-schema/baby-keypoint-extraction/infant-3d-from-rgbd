import numpy as np
import pandas as pd

from src.detection.mediapipe_pose_detector import MPModel
from src.utils.path import get_root_path


def extract_diff_from_origin(origin_file: str, data_file: str, diff_file: str):
    origin_df = pd.read_csv(origin_file)
    columns = ['image_x', 'image_y', 'x', 'y', 'z']

    # Filter rows where 'landmark' equals 'Origin' and calculate averages of specified columns
    average_values_origin = origin_df[origin_df['landmark'] == 'Origin'][columns].mean()
    print("Average values of origin landmarks:")
    print(average_values_origin)

    data_df = pd.read_csv(data_file)
    diff_df = data_df.copy()

    # Modify columns in diff_df to contain the differences between the data values and the averaged values
    diff_df[columns] = data_df[columns] - average_values_origin

    # In records where data_df[columns] where 0, set diff_df[columns] to 0
    for key in columns:
        diff_df.loc[data_df[key] == 0, key] = 0

    # Save the resulting differences to a new CSV file named D435_diff_corrected.csv
    diff_df.to_csv(diff_file, index=False)


if __name__ == "__main__":
    # camera_name = "D435"
    # camera_name = "D455"
    camera_name = "ZED"

    baby = "NT"
    age = "27w"
    parent_folder = f"{get_root_path()}/output/{baby}/{age}/{camera_name}"

    origin_file = f"{parent_folder}/{camera_name}_origin.csv"

    model = MPModel.NATIVE
    if model == MPModel.VITPOSE:
        data_file = f"{parent_folder}/{model}/vitpose_out.csv"
        diff_file = f"{parent_folder}/{model}/vitpose_out_diff.csv"
    elif model == MPModel.NATIVE:
        data_file = f"{parent_folder}/{model}/{camera_name}_short_{model}.csv"
        diff_file = f"{parent_folder}/{model}/{camera_name}_short_{model}_diff.csv"
    else:
        data_file = f"{parent_folder}/{model}/{camera_name}_short_{model}.csv"
        diff_file = f"{parent_folder}/{model}/{camera_name}_short_{model}_diff.csv"

    extract_diff_from_origin(origin_file, data_file, diff_file)
