# Camera-Thesis

## About

This project is used to compare state-of-the-art stereo RGB-D cameras from two manufactures, namely Realsense and ZED.
Together with evaluation of several models; MediaPipe and ViTPose.
The goal is to find the best camera for our use case.

## Getting started

Before we start, we need to install ZED SDK and Realsense SDK as follows:

### ZED SDK
1. Download ZED SDK from [here](https://www.stereolabs.com/developers/release/).
   * Be aware, that ZED SDK is supported only together with CUDA. Therefore, only with NVIDIA GPU.
   * If you don't have NVIDIA GPU, you can still jump to Realsense SDK installation.
2. Install ZED SDK according to the [documentation](https://www.stereolabs.com/docs/app-development/python/install/).
   * It is important that you build the wheels for your python version and architecture.
     * After the installation, you should see in the current directory three built wheels similarly like this:
       ```
       PyOpenGL_accelerate-[VERSION]-cp[PYTHON_VERSION]-cp[PYTHON_VERSION]-win_amd64.whl
       PyOpenGL-[VERSION]-cp[PYTHON_VERSION]-cp[PYTHON_VERSION]-win_amd64.whl
       pyzed-[VERSION]-cp[PYTHON_VERSION]-cp[PYTHON_VERSION]-win_amd64.whl
       ```
   * During the SDK installation, you also built binaries of such SDK. Such binaries are necessary for later usage in your other virtual environment.
   * To work with new virtual environment, use the following steps:
     * Create your new virtual environment, e.g. `python -m venv .venv`
     * Activate your new virtual environment, e.g. `.venv/Scripts/activate`
     * Install **ALL** the built wheels, e.g. `pip install PyOpenGL_accelerate-[VERSION]-cp[PYTHON_VERSION]-cp[PYTHON_VERSION]-win_amd64.whl`
     * Find the binaries in your ZED SDK installation folder.
     * They are usually located in `ZED_SDK/bin/`
     * Based on your HW, they can be found as e.g. `ZED_SDK/bin/sl_ai64.dll` and `ZED_SDK/bin/sl_zed64.dll`
     * Copy them into your new virtual environment, e.g. `.venv/Lib/site-packages/pyzed/`
     * After that, you should be ready to go...

### Realsense SDK
1. Follow the [documentation](https://www.intelrealsense.com/sdk-2/) to install Realsense SDK.
2. The easiest way to download SDK into your virtual environment is through [pip](https://www.w3schools.com/python/python_pip.asp). `pip install pyrealsense2`


## Setup the environment
For better reproducibility, we also provide `requierments.txt` file with all necessary dependencies.
1. After you clone our repository
2. Install dependencies in root directory with `pip install -r requirements.txt`
3. If any package fails, look for version mismatch between your python version and its package version.

## Run
In order to run the application, you can choose from several scripts from pure streaming `streaming/cameras/camera.py` 
to running mediapipe or zed native `detection/pipelines/` or evaluation scripts upon CSV files in `evaluation/`.

Because the hierarchy of the project is quite complex and was used primarily for Thesis purposes, we recommend to use 
contact the author for further guidance.